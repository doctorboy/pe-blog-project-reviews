Hey everyone,

This is officially the home of all of the IC/GB scorecards review for keyboard projects that doctorboy is doing in his [blog](https://perifericosdeescrita.substack.com). 
All scorecards and blog posts are being written in Portuguese, and there is no intention to translate them into English for now.
This is part of the effort to create more engagement with the mechanical keyboard Portuguese community.
If you are new to these scorecards, please check out the Example Score Sheet under the template folder, which will outline each category and introduce the typical layout 
format. All files are written in pure markdown text for better readability and information storage. The scorecard format will likely evolve, so please expect some changes 
and adjustments as we move forward.

This folder is expected to be updated weekly at the same pace as blog posts are written. For now, there will not be an effort to review past scorecards and update them 
with newer formats. I will leave that for when the project is revisited due to some non-related reason.

If you see any typos and want to collaborate, please DM doctorboy directly or submit a pull request.  
I appreciate your understanding.

--  
doctorboy

## Useful links

- Blog (in portuguese): https://perifericosdeescrita.substack.com
- ClackX website: hhtps://clackx.xyz
