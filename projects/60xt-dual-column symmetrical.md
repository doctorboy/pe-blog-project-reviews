# 60XT
Um 60% com duas colunas extra que é simétrico

![](assets/images/20230530-project-60xt-dual-column%20symmetrical.jpeg)

**Data do último update:** 30/05/2023
**Maker:** Bowlkeyboards

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120409.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0.5     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 1     |
| **Total**            | **6.5+1** |

### O que aprecio

- _Layout_ fora do comum, que privilegia simetria, não despendendo de teclas macro extra.
    - Layout disponível em WKL, WK e HHKB.
- Utilizar alumínio 6063.
- Um _weight_ interno e externo com material mais nobre, neste caso, latão.
- Pormenor de ter a USB _daughterboard_ e respectivo conector escondido, para haver um menor impacto na acústica do teclado.
- Adoção do novo conector _standard_ para a PCB e USB _daughterboard_.
- Ao nível do _mounting_, uma combinação de _gaskets_ com _leafspring_, deve, na teoria, proporcionar um bom resultado.
- 9º graus de inclinação, é pessoalmente a minha inclinação favorita.
- Excelente desenho a mostrar detalhadamente a altura do teclado (ponto bónus).
- _Proxy_ na Europa pelo melhor vendedor neste momento, EloquentClicks.
- O design do _profile_, é muito interessante e acho pessoalmente bastante bonito.

### O que não aprecio

- Na parte inferior do teclado, os _bumpons_ não têm local próprio que ajude estes a manter a sua posição.

### O que gostava de ver alterado

- Utiliza uma _daughterboard_ de um fabricante conhecido, Geonworks, mas preferia que fosse alterado para uma das versões _open-source_. No entanto, é melhor que ter uma versão 100% própria.
- Aprecio a opção de ter uma _plate_ especifica para ISO, mas gostava que houvesse também a alternativa de ter uma plate universal para quem quiser utilizar os dois layouts e não ter a necessidade de comprar duas _plates_.
- Gostava de ver mais fotos da parte interior, visto que existem protótipos. Mas é provável que ainda não esteja com a qualidade pretendida.
- Mais informação sobre a PCB, ou algumas imagens (ex. tem _flex cuts_?.
- Gostava que fossem utilizados um _bumpons_ melhorzinhos.
- O preço, apesar de um pouco elevado, parece ser ajustado para o _design_ e materiais utilizados.


## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.