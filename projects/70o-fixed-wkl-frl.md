# 70o

![](assets/images/20230531-project-70o-fixed-wkl-frl.jpeg)

**Data do último update:** 31/05/2023
**Maker:** @Thebloodyez (Geekhack)

## Interest Check

IC realizado antes desta iniciativa.  

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120383.0)

### PROS

- Acho interessante a tentativa de tentar algo diferente e aplicar novas técnicas para melhorar _mounts_ menos populares, como o _sandwich_ mount.
- Ter um _weight_ em latão interno.
- Suporte para diversos _layouts_, que sejam WKL, incluindo ISO, _split_ _backspace_ e uma barra de espaços 10u.
    - Suporte de Alps também é bastante positivo.
- Ao nível de firmware, suporte de QMK/VIA/VIAL
- Uso de uma USB _daughterboard_ _open source_ (Unified C3).
- A _plate_ não ter _meme cuts_ e ter um _design_ bastante sólido.
- A implementação do _mounting_ em _leafspring_ parece ser bastante boa.
- Projecto com supervisão e suporte de vários elementos de renome na comunidade, são um selo de qualidade sobre os diversos componentes deste teclado.

### CONS

- O facto de não ter um vendedor europeu, torna a sua compra bastante dispendiosa e desproporcional para o projecto em questão, o que é uma pena.
- Oportunidade perdida de esconder o _cutout_ do cabo JST com o _weight_ visto que estão bastante próximos.
- Um pormenor: oferecer apenas uma opção de PCB com 1.2mm. Para um teclado deste género, o ideal era que se oferece uma medida _standard_ de base e depois como opção a versão 1.2mm.

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O 70o é um projecto bastante sólido, que utiliza um formato TKL FRL e tenta melhorar um tipo de _mounting_ que é muito popular em teclados _stacked_ _acrylic_, o _sandwich_ _mount_. O criador trabalhou em conjunto com diversos elementos de renome na comunidade para obter os melhores resultados finais, e pessoalmente diria que atingiu com sucesso esse objectivo. O ponto fraco, a meu ver, neste teclado, é o seu design, onde as linhas são muito básicas e com um look muito antiquado, e que são já muito vistas nos dias actuais. O preço é um pouco elevado (por ser poucas unidades) e o facto de não haver um vendedor europeu, torna a sua compra bastante dispendiosa e desproporcional para o teclado que é. 

**Nota Extra**: o criador deste projecto, no dia 31/05 **cancelou o GB** devido ao fraco volume de vendas.

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.