# 825 TKL
Um TKL com a temática de Hong Kong

![](assets/images/20230523-project-852-tkl.jpeg)

**Data do último update:** 23/05/2023
**Maker:** dro0pbear

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120281.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **7** |

### O que aprecio

  * A escolha de uma PCB open source para o teclado que utiliza também uma USB-C _daughterboard1_ _open source_ (a PCB suporta ISO).
  * Três opções distintas para o _mounting_.
  * Opção por algo diferente ao nível de _plates_, com opções chamadas hibridas onde dois materiais são combinados para os _mods_ e teclas alfanuméricas. Os ficheiros irão ser disponibilizados depois do GB para os utilizadores poderem explorar outros materiais.
  * Mais um projecto que por defeito não recorre à espuma e o criador preocupa-se em criar algo próprio.
  * O _weight_ interno está posicionado na zona das teclas alfanuméricas. Mais um pormenor interessante para controlar melhor a uniformidade do som nas teclas alfanuméricas.
  * Após feedback e a realização do primeiro protótipo, a alteração do _cutout_ para o cabo JST da PCB que será feito debaixo do _weight_, mais uma vez para minimizar algum distúrbio sonoro que possa existir.

### O que não aprecio

  * Pelas fotos esperaria um teclado com um peso acima dos 2.5Kg, mas segundo as especificações tem apenas aproximadamente 2Kg, o que acho um valor um pouco baixo.
    * Após ver as fotos do projecto no site _imgur_, percebi que o _weight_ está dividido em duas peças finas, uma no interior e outro no exterior, mas que reduz bastante o tamanho do _weight_ e o respectivo peso do teclado.
  * O preço alvo ($450) está ligeiramente elevado, mas diria que é aceitável (assumindo tipo um número mínimo de unidades na casa de 100, visto que este número não é mencionado). Se não tiver proxy na EU, o preço ficará demasiado elevado.

### O que gostava de ver alterado

  * Gostava que em vez de dois _weights_ finos, um na parte interior e outro na parte exterior, a escolha tivesse recaído num _weight_ maior, que estivesse exposto simultaneamente no interior e exterior do teclado.



  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.