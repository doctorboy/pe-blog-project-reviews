# 917.2

![](assets/images/20230530-project-917-r2.jpeg)

**Data do último update:** 30/05/2023
**Maker:** treeleaf, design do Hali e PCB do sleepdealer

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120382.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **5** |

### O que aprecio

- Mais unidades para serem vendidas de um teclado que teve muito poucas unidades à venda, oferecendo desta maneira nova oportunidade para mais pessoas comprarem este teclado muito particular.
- Adicionada opção de PCBs com suporte a Alps.

### O que não aprecio

- Não terem sido feitos quaisquer tipos de melhoramentos.

### O que gostava de ver alterado

- Nada a assinalar.

  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.