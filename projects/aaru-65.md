# Aaru65

![](assets/images/20230715-project-aaru-65.jpeg)

**Data do último update:** 15/07/2023
**Maker:** Helix Lab

## Interest Check

IC realizado antes desta iniciativa.    

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120752.0)

### PROS

  * Design e acabamento bastante único e com muitos detalhes. Mesmo para quem não é fã do _design_, é muito difícil ficar indiferente à estética deste teclado.
    * Os hieróglifos utilizados são os que estão presentes na 6ª e 7ª linha da _[Rosetta Stone](https://en.wikipedia.org/wiki/Rosetta_Stone)_.
    * De notar que nas partes interiores a qualidade do acabamento não é a melhor (são visíveis marcas de maquinação). Fica a nota para todos os que privilegiam também uma qualidade irrepreensível no interior do teclado.
  * A versão _Divine_, tem dupla anodização para diferenciar e destacar os hieróglifos na parte superior.
    * Destacar também a variedade de opções disponível no _group_ _buy_.
  * Utilização de um botão _slider_ vertical, que não é muito comum no _hobby_ (recentemente já tínhamos visto um teclado com este tipo de botão, o [Vanguard65](https://perifericosdeescrita.substack.com/i/123574171/vanguard)).
  * Considero o preço de $430 algo relativamente alto para um 65% normal (ainda mais a versão _Divine_, que custa $580) mas o Aaru 65 está longe de ser um teclado normal. Se tivermos em conta o _design_ e o acabamento presente, considero o preço bastante interessante, nada de incrível, mas completamente justificável.


### CONS

  * Alguns desafios ao nível de software no que diz respeito ao botão _slider_ vertical na lateral direita (ex. a função por defeito de controlar o volume do som não é suportada em Mac OS).
  * Um “65%” que na realidade é um 60% com _arrow keys_ e um _slider _vertical.
  * Não disponibilizar um vendedor europeu, tornando o preço final deste teclado em mercado europeu demasiado elevado.
  * _Plate_ com demasiados _cutouts_ (ver [review](https://www.youtube.com/watch?v=O0kKt6TL1FY) extensa do Blacksimon onde é mencionado que devido a estes cortes, é necessário usar uma peça de silicone, fornecida com o teclado, entre a PCB e a _plate,_ para dar o suporte que falta na _plate_).
  * O processo de montagem de todo o teclado, não é trivial e pode ser frustrante para os utilizadores mais novatos no _hobby_ (ver [review](https://www.youtube.com/watch?v=O0kKt6TL1FY) extensa do Blacksimon para mais detalhes).
  * USB _daughterboard_ não é um modelo _open-source_ (um pouco incompreensível, quando já estão a utilizar um dos _form_ _factors_ dos modelos _open_ _source_).


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O Aaru 65 segue a mesma linha de design e temática do Aaru TKL, um teclado com muita personalidade e um visual único. Pessoalmente não sou um fã da temática utilizada, mas é impossível ficar indiferente ao design apresentado pelo Aaru 65. Os hieróglifos são absolutamente incríveis, quer ao nível dos detalhes apresentados como a maneira como são “desenhados” em relevo na superfície do teclado. Se juntarmos a isto a possibilidade de ter dupla anodização onde estes hieróglifos ficam na cor dourada, o aspecto visual final do teclado é incrível. Para além desta estética muito diferenciada, destacar ainda o facto do Aaru 65 oferecer um inovador botão _slider_ vertical, que na teoria serviria para várias funções. Digo na teoria, porque, na prática, este _slider_ contém _hardware_ que ainda não é suportado no _firmware_ QMK, tornando a sua utilidade um pouco questionável. Infelizmente, esta não é a única escolha que a Helix Labs realizou na criação deste teclado que não faz grande sentido para mim: a USB _daughterboard_ que tem o mesmo _form_ _factor_ de modelos _open_ _source_, mas depois é usada uma PCB proprietária ou a _plate_ cheia de _meme_ _cuts,_ são apenas algumas destas decisões. Pessoalmente, num teclado _premium_ e com um _design_ tão único, esperava que as decisões tomadas fossem mais assertivas e tornassem este Aaru 65 uma referência no mercado. Se é fã da temática, aconselho a compra deste teclado bastante único, pois em termos de estética é uma peça diferenciada e deslumbrante. Só tenho pena que o resto do teclado não acompanhe esse mesmo nível elevado.

  



[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.