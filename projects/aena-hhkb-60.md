# Aena
Subtitulo

![](assets/images/20230605-project-aena-hhkb-60.jpeg)

**Data do último update:** 05/06/2023
**Maker:** Cofcream

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120443.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 0.5     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 0     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **5.5** |

### O que aprecio

  * Um teclado focado num _layout_ que não tem a atenção devida, o _layout_ HHKB.
  * O _weight_ disponibilizado, com dimensões assinaláveis, em aço inoxidável com acabamento espelhado.
  * Usar alumínio 6063 na _case_.
  * Oferecer dois tipos de _mounts_ com diferentes objectivos, um para uma sensação de escrita mais suave e outra para uma sensação mais rija.
  * Uso de uma USB _daughterboard_ _open source_.

### O que não aprecio

  * Não ser um HHKB verdadeiro, o que não parece fazer muito sentido quando esse _layout_ é o _layout_ base do produto.
    * Tenho algumas dúvidas se suporta, por exemplo, _split-backspace_.
  * O preço estimado de $440, para um MOQ de 300 unidades. Acho o preço desajustado para o que o teclado oferece.
    * Provavelmente o preço é mais elevado por este ser constituído por 3 peças, incluindo o _weight_ de dimensões generosas.

### O que gostava de ver alterado

  * Falta muita informação ainda sobre o projecto.
  * Considerando que são 300 unidades, era interessante que houvesse mais opções e não apenas uma configuração.
  * Gostava que o projecto tivesse um MOQ mais baixo, pois não acredito que com um MOQ de 300, este valor seja atingido considerando o preço alvo e restantes características.
  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.