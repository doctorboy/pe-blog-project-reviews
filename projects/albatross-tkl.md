# Albatross TKL

![](assets/images/20230818-project-albatross-tkl.jpeg)

**Data do último update:** 18/08/2023
**Maker:** Ikeygai Designs

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=121091.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0.5     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **6.5** |

### O que aprecio

  * Acho interessante a utilização de uma _plate_ dividia em dois, para tentar manter uma melhor uniformização no feeling em todo o layout.
  * O uso de um _weight_ que é externo e interno ao mesmo tempo.
  * Utilização de uma USB _daughterboard_ _open-source_.
  * PCB desenhada por um criador reputado no _hobby_, o Gondo.
  * O design e posicionamento dos _mounting points_ na _plate_ parecem ser interessantes.
  * Suporte de ISO com a _solderable_ PCB.

### O que não aprecio

  * A mensagem contraditória de que o Albatross foi refinado para ter um “bom” som e depois dizer que vem por base com várias opções de espuma.
  * O _design_ da porta USB que não me parece ser compatível com muitos dos cabos no mercado.

### O que gostava de ver alterado

  * O _weight_ interno foi elaborado para representar o símbolo da marca. Mais uma vez parece-me que a mensagem de que o teclado foi refinado para ter um “bom” som cai um pouco por terra.
  * O preço ($350) não me parece ser o melhor tendo em conta o que o teclado oferece. Mas é difícil de avaliar quando não se sabe qual o MOQ do projecto.
  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.