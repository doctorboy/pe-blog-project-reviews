# Aquila 60%
A 60% wireless keyboard

![](assets/images/20220510-project-aquila60.jpeg)

**Data do último update:** 11/05/2023
**Maker:** Kevin from Kindlestar

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120252.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 0.5   |
| Protótipos           | 1     |
| Mounting visível     | 0     |
| Layouts suportados   | 0     |
| Preço Estimado       | 0     |
| Bónus                | 0     |
| **Total**            | **3.5** |

### O que aprecio

-   Um design fora do comum (apesar de não o apreciar pessoalmente).
-   A qualidade da PCB parece ser acima da média, e acho interessante existir a opção de per-key RGB o que é raro no hobby (apesar de ser limitador ao nível de layouts e achar que não faz grande sentido para o utilizador entusiasta que usa sets GMKs e afins, onde as teclas não deixam passar a luz).

### O que não aprecio

- Solução sem fios proprietária com design próprio. É preciso perceber qual é o impacto desse design depois a nivel compatibilidade com software open source.
- Algumas fotos com perfumes da Dior? Porquê? E algumas fotos sem cuidado na apresentação, tipo com a espuma toda torta.

### O que gostava de ver alterado

-   Mais informação sobre o projecto e não simplesmente algumas imagens e umas specs soltas. Falta informação sobre o mounting (pela PCB parece ter um mounting point lateral, o que é estranho), sobre layouts suportados, design das plates, etc.
-   Pela imagem da PCB, só vemos uma versão hotswap com suporte a ANSI: gostava de ver ISO e eventualmente uma PCB sem ser hotswap.
-   Inconsistência nos cutouts nos alphas na PCB, que potencialmente irá criar diferenças ao nível de som e de feeling.
-   Por já existirem protótipos, gostava de ver uma referência ao preço alvo do teclado.
-   Mais fotos sobre as partes internas (existe um weight interno?).


## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=121096.0)

### PROS

  * _Design_ completamente fora do comum, ou se gosta ou se odeia.
  * Apesar de utilizar uma solução sem fios proprietária, a PCB parece ser compatível com VIA (e consequentemente com QMK), o que são óptimas noticias.
    * A utilização de uma pequena antena externa para melhorar o sinal do _bluetooth_ é interessante, resta saber se melhora alguma coisa, na prática.
  * Após um IC que tinha muito pouca informação e poucas coisas eram explicadas, a apresentação do GB está num nível bastante bom, melhor que muitos projectos de criadores de renome.
  * Mesmo considerando que o teclado é todo em alumínio, o preço de 300€ parece-me ser bastante aceitável.
  * Está disponível num vendedor europeu (no entanto, é um vendedor pouco conhecido e pode apresentar alguns riscos).

### CONS

  * Apenas tem uma oferta de PCB e é hotswap com um _layout_ fixo (por exemplo, não suporta ISO ou barra de espaços 7u).
  * Tem PCB _mounting_ _points_ para usar sem _plate_, mas depois apenas oferece uma PCB com *hotswap*. Não faz sentido.
  * Não utiliza uma USB _daughterboard_ _open_-_source_.
  * Não tem _weights_ e apenas utiliza uma espécie de _badge_ na parte inferior (no lado positivo, utiliza alumínio 6063).
  * Os _cutouts_ na parte interior e o respectivo compartimento para a bateria, criam na teoria uma não uniformidade para o som em toda a zona das teclas alfanuméricas. Pode ser um pouco problemático se não for utilizada espuma, mas fica a nota que na review do Alexotos não notei nada.

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [x] Não recomendo

**Comentário:** O Aquila 65 (para mim é um 60% com teclas direcionais; aliás, o nome original foi mesmo Aquila 60) é um projecto com um _design_ bastante arrojado e que se foca essencialmente em oferecer uma proposta diferente e divertida. Mesmo não sendo algo que pessoalmente aprecie esteticamente, acho que existe muito valor num criador lançar um projecto desta natureza. Dizer ainda, que o preço de 300€ parece-me perfeitamente ajustado para o que é oferecido neste projecto para um público alvo que será sempre de pouca dimensão. No entanto, foi com pena que vi que o teclado acaba por falhar em muitos outros aspectos que considero relevantes e que me levam a não recomendar o Aquila. Caso os pontos que tenha referido na secção dos aspectos negativos não sejam um problema e caso gostes do _design_, o Aquila 65 certamente será um teclado divertido de usar e provavelmente vai deixar muitos dos teus colegas e familiares completamente perplexos, pois o _design_ não deixa ninguém indiferente. 

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.