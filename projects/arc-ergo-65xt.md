# Arc Ergo 65xt

![](assets/images/20230613-project-arc-ergo-65xt.jpeg)

**Data do último update:** 17/07/2023
**Maker:** Shostudios (antigo designer e co-fundador da Hex Keyboards)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120546.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 0.5     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **4.5** |

### O que aprecio

  * Ser um teclado ergonómico, sem ser o layout normal "alice/arisu".
    * Em termos de *design* tem algumas parecenças com o "Boomerang". De realçar que o criador falou anteriormente com o autor do "Boomerang" para esclarecer quaisquer suspeitas de plágio. Além disso, alegadamente existem indícios de este projecto ter arrancado em 2021.
  * Uso de madeira na parte frontal.
  * Alumínio 6063.
  * _Weight_ interno em latão.
  * Suporte de QMK e VIAL.
  * Já ter havido duas rondas de protótipos.

### O que não aprecio

  * Não ter IC _form_ para recolher _feedback_ de modo estruturado.
  * O preço alvo de $468 parece ser um pouco puxado, mas para um MOQ de 50 unidades e considerando o formato do teclado, não há milagres. De referir que o MOQ parece ser bastante realista.
  * Não são referenciados distribuidores ou revendedores, como tal para terras lusas deve requerer importação.

### O que gostava de ver alterado

  * Gostava de perceber a ideia por trás de ter alumínio 6061 na _plate_ quando esta prática não é muito comum.

## Group Buy

Link: [Página do GB](https://shostudios.co/products/arc-an-ergonomic-65-with-macros), [Review do IC](https://perifericosdeescrita.substack.com/i/128035720/arc-ergo-xt)

### PROS

  * Um _layout_ ergonómico fora do convencional.
  * Uso de madeira é um toque de requinte interessante.
  * Utilização de materiais mais _premium_, como alumínio 6063 na _case_ e latão no _weight_.
  * Suporte de QMK e Vial.
  * Utilização de uma USB _daughterboard_ _open_ _source_.
  * Não usar espuma e ter sido desenhado em torno desse conceito.

### CONS

  * Não haver muita informação e imagens das partes interiores do teclado que permitam uma melhor avaliação do mesmo.
  * O preço de $480 não é o mais competitivo, mas diria que tal se deve ao facto de estar a ser projectado um baixo número de vendas (no IC era mencionado um MOQ de 50 unidades).
    * O peso de 2Kg, já montado, considerando os weights, parece-me ser um pouco baixo. Leva-me a crer que os weights interiores são pequenos.
  * Para agravar mais o ponto anterior, este teclado não é disponibilizado por nenhum vendedor europeu, o que obriga a importação caso o queiramos adquirir. Este facto eleva o custo final do teclado em mercado europeu para valores perto dos 600€.

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O Arc Ergo 65% é um teclado ergonómico com um design bastante apelativo e com pequenos toques de requinte interessantes, como, por exemplo, usarem um friso de madeira no perfil frontal do teclado. Acho que o projecto tem bastante potencial, e é uma pena que não tenha sido disponibilizada mais informação sobre o mesmo, em particular alguns detalhes sobre a parte interior. Apenas [neste vídeo](https://www.youtube.com/watch?v=-bQCufBusxE), consegui observar algumas das partes interiores como, por exemplo, a utilização de um _split_ o-_ring_ _mount_. O preço não é o melhor, principalmente se tivermos em conta que requer custos de importação em território europeu, mas não deixa de ser um projecto interessante para todos os amantes de _layouts_ ergonómicos. Se a tua preferência é mais na linha dos 60%, existe um projecto semelhante que provavelmente será superior ao Arc a ser preparado pela Laminar Designs, o [Boomerang](https://geekhack.org/index.php?topic=119105.0). No entanto, se 65% é o teu _layout_ favorito, o Arc mesmo não apresentando o melhor preço, é um projecto interessante.

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.