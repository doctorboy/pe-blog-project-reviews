# Augur R2

![](assets/images/20230605-project-augur-60-r2.jpeg)

**Data do último update:** 5/06/2023
**Maker:** JLabs

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120437.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0.5     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **6.5** |

### O que aprecio

  * Indicar o que foi alterado da primeira ronda de vendas para esta segunda ronda.
  * Mais opções de personalização vão estar disponíveis para o _group-buy_.
  * A utilização de uma USB _daughterboard_ _open-source_.
  * Informação e fotos de várias partes internas do teclado.
  * O preço parece ser interessante, no entanto, a concorrência é forte em teclados com estas características.
    * Um MOQ de 200 unidades pode não ser fácil de atingir presentemente, principalmente quando existe bastante competição.
  * Os preços dos extras também são bastante competitivos.

### O que não aprecio

  * O _weight_ ser apenas uma placa fina (é, no entanto, em latão).
  * Não suportar ISO.
  * Apenas oferecer plates a suportar ANSI e barra de espaços 7u. Se o teclado é publicitado nos layouts a suportar barra de espaços 6.25u, faria sentido oferecer uma plate que suportasse esse layout (nem que seja como extra).

### O que gostava de ver alterado

  * Actualizar a especificação da USB _daughterboard_ que utiliza. (Usar a versão C4 que já usa o novo conector).
  * Que tivessem efectuado melhorias no teclado em si, nesta nova ronda. Ou um retoque qualquer próprio na parte estética.
    * Por exemplo, podiam cobrir nesta ronda o _cutout_ para o cabo da USB _daughterboard_, para na teoria melhorar a parte acústica.
  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.