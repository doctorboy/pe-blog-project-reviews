# Berceste WKL 65%
Subtitulo

![](assets/images/20230802-project-berceste-65-wkl.jpeg)

**Data do último update:** 02/08/2023
**Maker:** Furki

## Interest Check

IC realizado antes desta iniciativa.  

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120887.0), [Oblotzky Industries](https://oblotzky.industries/products/berceste)

### PROS

  * Um _mounting_ _plateless_ para os amantes de 65%.
  * _Weight_ externo.
  * Uso de materiais premium como alumínio 6063 e um _weight_ em aço inoxidável.
  * Suporte de ANSI e ISO.
  * _Design_ com alguns pormenores interessantes como os HG _sides_ e o _lip_ na parte frontal.
  * Fabricado numa das melhores fábricas de momento de teclados com índice de qualidade elevado.

### CONS

  * Falta-me alguma informação que costuma estar presente em GB: imagens da PCB, _daughterboard_, etc.
  * Não aprecio o _design_ do _leafspring_ realizado. Penso que é demasiado frágil e com o tempo poderá levar a problemas.
  * O preço (320€ sem VAT) parece-me um pouco elevado para o que oferece, mas tudo dependerá no MOQ em questão e do tamanho do _weight_, que me parece ser de dimensões pequenas.

### Recomendo comprar?[^1]

- [x] Sem dúvida
- [x] Sim, se...
- [x] Não recomendo

**Comentário:** O Berceste é um teclado que infelizmente chega um pouco tarde ao mercado para ter o sucesso que o criador pretendia. O Berceste tinha alguns pontos interessantes ao nível de _design_ e era um projecto europeu, o que é sempre de valorizar. Digo era, porque o criador nos primeiros dias do GB já anunciou que o projecto do Berceste não vai para a frente, devido ao volume baixo de vendas. Esperemos que o criador continue a explorar o _hobby_ e nos consiga oferecer melhores propostas no futuro.


[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.