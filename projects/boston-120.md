# Boston 120%

![](assets/images/20230627-project-boston-120.jpeg)

**Data do último update:** 27/06/2023
**Maker:**

## Interest Check

IC realizado antes desta iniciativa.    

## Group Buy

Link: [Página Oficial](https://rndkbd.com/products/gb-boston-120-compact-keyboard-kit?variant=44074927194346), [IC original no Geekhack](https://geekhack.org/index.php?topic=106501.0)

### PROS

  * Nova ronda de venda de um teclado desenhado em 2020, para os amantes de teclados superiores aos 100%.
  * O preço base é competitivo tendo em conta que o MOQ é bastante baixo (25 unidades).
  * Segundo a página que contém as especificações, utiliza a nova versão da USB _daughterboard_ _open_ _source_.

### CONS

  * Não tem vendedor na Europa, o que torna o preço de aquisição mais elevado.
  * Não _weights_ internos. É um design simples, mas de grandes dimensões.
  * Não tem o _engraving_ do mapa de Boston na parte inferior do teclado como as versões anteriores.
    * "O Boston deixou de ter Boston".
  * Não é disponibilizada informação e fotos das partes internas aquando da compra.
  * Ao ver as fotos da PCB na página dos extras, notei que esta inclui uma porta USB-C. Fiquei então um pouco confuso, visto que nas definições do teclado, é mencionado que este utiliza uma USB _daughterboard_. Algo a rever.
  * Não é um contra por si, mas algo a ter em atenção: a PCB _hotswap_ necessita de soldar alguns componentes. Se quiser uma PCB pronta a usar, é necessário pagar um serviço extra para soldar esses componentes.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** Pessoalmente, eu sou sempre a favor de novas rondas de vendas de teclados que tenham tido sucesso ou por despertarem simplesmente novamente interesse na comunidade. O Boston 120% é um teclado que impressiona pelo seu tamanho e que certamente não deixará indiferente os amantes de _layouts_ extensos, como 100% ou acima. Embora o Boston 120% não ofereça alguns dos recursos de luxo encontrados em outros teclados atuais e tenha perdido uma das suas características, o _engraving_ do mapa de Boston na parte inferior, o objectivo continua a ser em oferecer uma boa proposta de valor num teclado de grandes dimensões. Pessoalmente, acho que o objectivo foi atingido com sucesso, e por pouco mais de $620 de preço base, podemos ter um 120% que não deixa ninguém indiferente. Para nós Europeus, tal como seria de esperar num projecto destas dimensões, não temos vendedor directo o que torna a sua compra bastante mais dispendiosa, mas mesmo assim, acredito que para um entusiasta de _layouts_ superiores a 100%, esta poderá ser uma oportunidade a não perder.


[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.