# Calla

![](assets/images/20230717-project-calla-tkl.jpeg)

**Data do último update:** Adicionar data
**Maker:** kevinave (Geekhack)

## Interest Check

IC realizado antes desta iniciativa.

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120827.0)

### PROS

  * Um TKL F13 WKL com um _design_ simples.
    * É interessante notar que há um _engraving_ na parte superior que pode não ser do gosto de todos.
  * Dois tipos de _mount_ disponível, _top_ _mount_ e o-_ring_ _gasket_ _mount_.
  * Disponível suporte para Topre/switches eletro-capacitivos.
    * Disponível EC kit aquando da venda.
  * Dois _weights_, um externo e outro interno.
  * Utilização de diversos tipos de material premium: alumínio 6063 na _case_, _weight_ interno em latão e _weight_ externo em aço inoxidável.
  * Suporte de QMK e Vial.
  * Suporte do layout ISO.
  * Utilização de uma USB _daughterboard_ _open_ _source_.
  * Não é um PRO por si só, mas o Calle tem um espaçamento entre teclas ligeiramente mais reduzido que o normal, 0.15mm a menos para ser mais preciso.
  * Após o GB, todo o projecto com excepção da _case_ irá ser _open_-_source_ (i.e. design das _plates_, _firmware_, ficheiros da PCB e _design_ dos _weights_).
    * Este ponto é importante devido à particularidade referida no ponto anterior,

### CONS

  * Os _weights_ são de pequenas dimensões, o que faz com que o peso final do teclado já montado seja na casa dos 1.8 Kg.
  * _Bumpons_ poderiam ser melhores, para um teclado de gama alta como este.
  * Não disponível com vendedor europeu, o que implica custos de importação aquando da sua aquisição.
    * O preço base começa nos $450 para a versão com _solderable_ PCB.

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O Calla é um TKL F13 com _layout_ WKL que tem dois argumentos de venda fortes: um espaçamento entre teclas que é ligeiramente mais reduzido, 0.15mm, o que torna a experiência de utilização ligeiramente diferente, e o suporte de switches Topre. Estas duas características juntas num TKL, torna este projecto um pouco mais especial para os amantes de TKLs, principalmente se tivermos em conta que a restante concepção do teclado parece ser bastante sólida e refinada. Destaque ainda para a postura do _maker_, ao indicar que após o GB, grande parte do projecto irá ser _open_ _sourced_. Relativamente ao preço, diria que é ligeiramente mais elevado do que o desejado, mas tendo em conta o projecto, diria que é o preço a pagar por se ter algo diferente com uma qualidade acima da média.

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.