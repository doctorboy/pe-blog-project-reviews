# Cat 65

![](assets/images/20230822-project-cat-65.jpeg)

**Data do último update:** 22/08/2023
**Maker:** Jacky e :3ildcat

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=121166.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 0     |
| Protótipos           | 0     |
| Mounting visível     | 0.5     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **4.5** |

### O que aprecio

  * Um 65% com um _mounting_ diferente do habitual.
  * A existência de um _weight_ externo em aço inoxidável.
  * Suporte de ISO na _solderable_ PCB.
  * Suporte de QMK e VIA
  * Oferta de uma _plate_ em fibra de vidro.

### O que não aprecio

  * O sistema de _mounting_ que utiliza molas levanta-me algumas dúvidas olhando para a imagem com a _exploded_ _view_. No entanto, o resultado final só poderá ser melhor avaliado com imagens de protótipos e vídeos.
  * Um teclado que tenta apresentar um sistema inovador para o _mounting_, mas que ainda não tem um protótipo para validar o conceito? Desapontante de ver, principalmente quando um criador como o :3ildcat está envolvido no projecto.
  * Mesmo sem saber o MOQ, diria que o preço alvo de $390 é demasiado elevado para o que é oferecido.

### O que gostava de ver alterado

  * Falta muita informação sobre muitas partes do teclado: design de partes internas, da plate, da PCB, etc.

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.