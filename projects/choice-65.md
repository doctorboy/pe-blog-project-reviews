# Choice65

![](assets/images/20230524-project-choice-65.jpeg)

**Data do último update:** 24/05/2023
**Maker:** Velocifire em nome da Cyberbay Studio

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120319.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 0     |
| Mounting visível     | 0     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **5** |

### O que aprecio

  * Tem como cor disponível, laranja, o que é fora do comum e interessante.
  * Tem um _weight_ em aço inoxidável e com diferentes tipos de acabamentos.
  * PCB disponível em versões _hotswap_ e _solderable_, sendo que esta última disponibiliza ISO.
  * Uso de alumínio 6063 na _case_.
  * O preço parece ser muito competitivo.

### O que não aprecio

  * Múltiplos níveis de espuma no interior do teclado.
  * O _mounting_ proposto não me convence na teoria, no entanto, poderá funcionar na prática. Para além desse facto, tenho receio que o "_plate pad_", que faz parte do _mounting_ proposto, interfira com a maneira como os _switches_ se fixam na plate.
  * Para um teclado com uma área para o _weight_ tão grande, o peso parece algo baixo. Concluo desse modo que o _weight_ deve ser bastante fino.
  * Não haver ainda protótipos ou imagens destes pelo menos.

### O que gostava de ver alterado

  * Falta saber mais informação sobre a PCB, se tem ou não _flex cuts_ e como estes estão feitos.
  * Falta informação sobre qual a USB _daughterboard_ a ser utilizada.
  * O _cutout_ para a USB _daughterboard_ parece ser um pouco despropositado. Noto também que a espuma tem _cutouts_ nessa área, o que à partida não faz muito sentido e criará inconsistências no som das teclas naquela zona.


## Group Buy

Link: [Choice65](https://geekhack.org/index.php?topic=120939.0), [KeyGem, vendedor EU](https://keygem.com/products/choice65-keyboard-kit-group-buy)

### PROS

  * Trás por defeito PCB _hotswap_, o que para uns pode ser visto como uma desvantagem, para mim faz todo o sentido tendo em conta o público alvo deste teclado.
  * PCB sem _flex_ _cuts_.
  * Utilização de USB _daughterboard_ _open_ _source_ (requer confirmação oficial).
  * Oferecer um _mounting_ diferente de outras opções no mercado.
  * O preço ser competitivo para o que oferece, 140€ no mercado europeu.
  * Disponível num vendedor europeu.

### CONS

  * O _weight_ é muito fino e de pequenas dimensões.
  * Jogada de _marketing_ ao oferecer uma opção que pode levar os mais distraídos a engano e pensar que o fundo do teclado é em cobre. É na realidade alumínio anodizado com cor de cobre.
  * Não sou fã, na teoria, do _mounting_ proposto que assenta na obrigatoriedade de utilizar-se a espuma. Um dia quando experimentar poderei dar uma opinião mais clara.

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O Choice65 é como o nome indica, mais uma escolha a considerar no mercado competitivo de 65%. O grande factor diferenciador do Choice65 é, no entanto, para mim, o seu maior ponto negativo, a utilização de um _mounting_ pouco comum no hobby. Não que isso seja mau por si, pessoalmente até acho que ter uma escolha diferente é algo muito positivo, mas o _mounting_ em questão assenta muito na obrigatoriedade de se utilizar espuma na montagem do teclado. Em termos de design, o Choice65 faz bastante uso da combinação de duas cores e tem linhas simples, mas modernas. Se gostas de 65% e procuras um teclado de gama de entrada que seja um pouco diferente do que o mercado oferece e tenha uma aparência interessante, o Choice65 pode muito bem ser o teclado que procuras. Se olharmos mais para a proposta de valor global, penso que existem outras propostas melhores.

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.