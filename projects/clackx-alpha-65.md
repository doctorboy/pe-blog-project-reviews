# ClackX Alpha

![](assets/images/20230803-project-clackx-alpha-65.jpeg)

**Data do último update:** 03/08/2023
**Maker:** doctorboy (ClackX)

## Interest Check
  
IC realizado antes desta iniciativa.
## Group Buy

Link: [Geekhack GB](https://geekhack.org/index.php?topic=120960.0), [Geekhack IC](https://geekhack.org/index.php?topic=119925.0), [Página do GB](https://clackx.xyz/products/clackx-alpha)

### PROS

  * Um 65% com um _layout_ diferenciado, com dois _knobs_ e um pequeno ecrã oled.
  * Uma PCB _hotswap_ com suporte de múltiplos _layouts_.
  * _Solderable_ PCB disponível como extra.
  * Uma _plate_ com _design_ próprio que aplica o conceito _leafspring_ a um _mount_ mais tradicional.
  * Utilização de uma USB _daughterboard_ _open_ _source_.
  * Suporte de QMK/VIA/VIAL.
  * Disponibilizado na Europa, sem custos extra de importação.
  * Devido ao _design_ da _case_, as possibilidades de modificar a estética são elevadas.

### CONS

  * A proposta de valor de um teclado deste género não é tão forte como a de um teclado com materiais mais _premium_, como, por exemplo, em alumínio.

### Recomendo comprar?[^1]

- [x] Sem dúvida
- [x] Sim, se...
- [x] Não recomendo

**Comentário:** Para todos os que não estão a par, o _Alpha_ é o meu primeiro projecto pessoal público no _hobby_ dos teclados mecânicos, como tal, a minha opinião não pode ser considerada 100% imparcial. O Alpha é um projecto que começou em 2020 e desde então já sofreu diversas iterações e melhoramentos até ao que é apresentado no dia de hoje. O grande aspecto diferenciador do Alpha é o seu _layout_ praticamente único e todos os pequenos melhoramentos que foram sendo feitos e que são praticamente invisíveis a olho nu. Um exemplo? É possível mudar a funcionalidade (desde que pré-programada) de um _knob_ sem utilizar qualquer tipo de _software_, o que é super pratico. Sim, sem sequer usar VIA ou VIAL. A utilização de um ecrã oled também ajuda bastante na usabilidade, porque permite a qualquer momento, por exemplo, ver quais as funcionalidades activadas nos _knobs_. Ou ainda ver qual é o RGB _mode_ que está a ser utilizado. Muitas vezes perguntam-me para que é que utilizo cada um dos _knobs_. Pessoalmente gosto sempre de ter o controlo do volume num dos lados e no outro um controlo para os _media_ _players_, onde possa fazer pausa/_play_ ou simplesmente mudar de música. Mas existem muitas mais opções.  
O Alpha foi o meu teclado do dia a dia durante mais de um ano, e mesmo hoje, apesar de ter outros teclados bastante _premium_, gosto sempre de o utilizar. Pessoalmente tenho perfeita noção que o preço é elevado para um _stacked_ _acrylic_, apesar de todos os meus esforços para baixar o preço. De realçar que o MOQ é de apenas 10 unidades, devido a ser um teclado para um nicho dentro já do próprio nicho. Com um MOQ bem mais elevado, certamente que o preço seria também bem mais apelativo, mas infelizmente tal não é possível.  
O Alpha foi um projecto desenvolvido durante 2 anos, com muito carinho e dedicação, e apenas com a ajuda de muitas pessoas é que hoje é possível estar disponível em GB. A venda termina no final do mês de Agosto e o número de unidades é limitado. Não existem planos para ter futuras rondas de venda do Alpha, portanto esta é uma oportunidade única de ter um teclado diferenciado e que ainda hoje apesar de estar longe de ser o meu melhor teclado, deixa-me sempre um sorriso no rosto quando o utilizo.

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.