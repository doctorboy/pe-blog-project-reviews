# Class60

![](assets/images/20230620-project-class-60.jpeg)

**Data do último update:** 20/06/2023
**Maker:** Minimal Makeover Studio

## Interest Check

IC realizado antes desta iniciativa.    

## Group Buy

Link: [Página oficial](https://www.mmkeyboard.com/collections/group-buy/products/mm-class60-retro-custom-mechanical-keyboard?variant=44085826814178), [Página na Keygem](https://keygem.com/products/group-buy-mm-class60-retro-keyboard-kit?variant=44485251334412)

### PROS

  * Um teclado bonito para todos os que apreciam um design mais retro.
  * Em termos de _layout_, suporte de WK, WKL e HHKB.
  * _Mounting_ com duas opções, _Top Mount_ e _Leafspring_.
  * Dois _weights_ em aço inoxidável com acabamento PVD, um externo e outro externo.
    * O pormenor de o _weight_ interno compartimentar a bateria, é interessante.
  * Compatibilidade e opção de comprar uma PCB para _switches_ eletrocapacitivos (i.e. Topre).
  * Opção com PCB _wireless_.
  * Opção de usar solenóide.
  * Preço bastante competitivo para o que oferece.

### CONS

  * Demasiada espuma no kit base. Contei cinco níveis de espuma.
  * Não usar uma USB *daughterboard* open-source.
  * Versão _wireless_ deve usar _software_ proprietário (não é mencionado compatibilidade com QMK no texto)
  * Não é claro sobre o que é incluído no kit para _switches_ eletrocapacitivos. É mencionado que inclui tudo menos as _keycaps_, mas não é claro.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** Se é amante de designs mais“retro”, e em teclados não é uma excepção, no tenho grandes dúvidas que o Class60 vai-lhe agradar. O Class60 apresenta um excelente preço já em mercado europeu, para o que oferece: teclado com dois pequenos _weights_ em aço inoxidável, várias opções de _layouts_, dois tipos de _mount_, solenóide e ainda a possibilidade de utilizar _switches_ _topre_. Em relação estes últimos, não sendo uma área que pessoalmente domine bem, não é muito claro para mim a fiabilidade do kit eletrocapacitivo disponibilizado e existe algum risco de no final poder apenas aproveitar a PCB. De referir ainda que este kit faz subir o preço significativamente, em mais de 50%, e coloca o valor global da compra num nível, no mínimo, discutível. De qualquer forma, e mesmo tendo algumas falhas, acho o Class60 um projecto interessante e com potencial para satisfazer os amantes de design mais “retro”.

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.