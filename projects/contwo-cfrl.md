# CONTWO CFRL

![](assets/images/20230808-project-contwo-cfrl.jpeg)

**Data do último update:** 08/08/2023
**Maker:** Kindakeyboards

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120997.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **7** |

### O que aprecio

  * Um IC bastante descritivo e completo. Cada vez mais raro presentemente.
  * O _layout_ fora do comum. Eu próprio já considerei no passado fazer algo parecido.
  * A utilização de um _weight_ que parece ter dimensões muito significativas.
  * A parte estética das PCBs é fora do comum.

### O que não aprecio

  * Os _cutouts_ da _plate_ para a PCB _solderable_ estão exagerados e não fazem muito sentido para mim.
    * Na PCB _hotswap_ estão melhores, mas ainda longe do ideal.
  * Pessoalmente não aprecio o mount, uma espécie de _gummy_ _o-ring, mas_ usando um _layer_ de “espuma”. Opinião pessoal.

### O que gostava de ver alterado

  * Custava de perceber qual o peso do teclado com o _weight_ proposto.
  * Os _cutouts_ para a USB _daughterboard_ parecem-me um pouco exagerados.
  * O preço de 410€-500€ é elevado, mas segundo as minhas estimativas isto é provavelmente um teclado para pesar mais de 3.5Kg, e se assim for, não acho muito exagerado para um MOQ 100.
  * Considerando o _layout_, a parte estética e o público alvo, não acredito que o MOQ seja atingido. Era preferível, a meu ver, fazer uma de duas coisas: ou cortar no _weight_/peso e apontar para um preço mais baixo, ou simplesmente assumir que se vai fazer algo _mid_/_high-end_ e apontar para um MOQ bem mais baixo, tipo na casa das 30 unidades.



## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.