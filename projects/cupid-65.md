# Cupid 65

![](assets/images/20230524-project-cupid-65.jpeg)

**Data do último update:** 24/05/2023
**Maker:** Chaosera

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120312.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 0.5     |
| Protótipos           | 0     |
| Mounting visível     | 0     |
| Layouts suportados   | 0     |
| Preço Estimado       | 0     |
| Bónus                | 0     |
| **Total**            | **0.5** |

### O que aprecio

  * _Design_ fora do tradicional, apesar de pessoalmente não ser um fã.
    * No entanto, acho bastante interessante as placas de titânio laterais com os desenhos.

### O que não aprecio

  * Deve ser dos ICs mais incompletos que vi nos últimos tempos, como demonstra a _checklist_ do IC.
  * Pouquíssima informação sobre as partes internas do teclado.

### O que gostava de ver alterado

  * Muitas cores disponíveis, mas os _renders_ não me parecem de todo realistas e representativos das cores finais (ex. laranja ou amarelo).
  * _Layout força o uso de duas teclas 2.25u, requerendo alguma atenção dos compradores se os _keysets que usam têm essa disponibilidade.
  * Falta informação do projecto a diversos níveis: info sobre as PCBs, _layouts_, _plates_, _mounting_, etc.


  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.