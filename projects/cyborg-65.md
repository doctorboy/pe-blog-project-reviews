# Cyborg 65%

![](assets/images/20230512-project-cyborg-65.jpeg)

**Data do último update:** 12/05/2023
**Maker:** Boom Lab, disponibilizado pela Keebclack

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120274.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 0     |
| Protótipos           | 0     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **5** |

### O que aprecio

-   Em termos de design, é algo fora do comum e ousado.
-   Acho interessante a ideia das luzes terem um efeito difuso, e não terem exposição directa para a visão do utilizador.
-   Ter os indicadores dos leds na zona do blocker.
-   O preço parece ser competitivo, para um 65%.
-   Ter diferentes tipos de mounting.

### O que não aprecio

-   Apenas ter PCB hotswap e Bluetooth. Falta uma solderable PCB com suporte a multi layout.
-   Haver diversos níveis de espuma quando parece não haver protótipos completos.

### O que gostava de ver alterado

-   Não me parece haver fotos dos protótipos, como tal vou assumir a não existência de protótipos. É importante fazer algo nesse campo antes do GB.
-   Ver informação sobre as especificações do teclado.
-   Ver informação sobre PCBs, Plates, etc.
-   Suportar ISO layout. De momento apenas suporta parcialmente ISO, falta o shift esquerdo pequeno e respectiva tecla extra.

### O que gostava de ver alterado

- -   Não me parece haver fotos dos protótipos, como tal vou assumir a não existência de protótipos. É importante fazer algo nesse campo antes do GB.
-   Ver informação sobre as especificações do teclado.
-   Ver informação sobre PCBs, Plates, etc.
-   Suportar ISO layout. De momento apenas suporta parcialmente ISO, falta o shift esquerdo pequeno e respectiva tecla extra.


## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.