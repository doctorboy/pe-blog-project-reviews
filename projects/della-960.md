# Della 960

![](assets/images/20230605-project-della-960.jpeg)

**Data do último update:** 05/06/2023
**Maker:** Della Key

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120481.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 0     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 0.5     |
| Bónus                | 0     |
| **Total**            | **3.5** |

### O que aprecio

  * A parte inferior do teclado que é na sua totalidade em cobre.
  * Usar alumínio 6063.
  * Peso de 4.8Kg sem estar montado.
  * O teclado irá ter provavelmente uma personalidade própria ao nível acústico (pode ser bom ou pode ser mau).
    * Ao ter uma peça no meio que tapa internamente o weight, o criador ambiciona atingir um som mais agudo, segundo o mesmo.
  * O design do perfil lateral parece ser bastante ousado. Tenho, no entanto, dúvidas sobre a sua possibilidade de criação.
  * Compatível com uma PCB bastante popular e de qualidade, como a Hiney H88nu.
    * Suportar ISO.

### O que não aprecio

  * O preço é bastante elevado, mesmo para um teclado deste género e com um MOQ de 30 a 50 unidades.
    * Indicar um preço alvo com um intervalo tão grande, como é de $1000 para $1500 não é o ideal.
  * A porta USB está numa posição elevada aumentando o perigo de estragar a porta USB.
    * No lado positivo, utilizará uma USB _daughterboard_.
  * Pessoalmente eu gostava que se tirasse mais proveito na parte interna de um _weight_ deste género. No entanto, o criador é claro que o objectivo é ter uma parte interior apenas com alumínio (na linha do que outros teclados no mercado já fazem como o Geonworks Glare TKL).

### O que gostava de ver alterado

  * Utilização de uma USB _daughterboard_ que seja _open-source_, o que não parece ser o caso de momento.
  * Gostava de ver um protótipo real para perceber o potencial deste projecto.




## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.