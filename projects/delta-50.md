# Delta 50%

![](assets/images/20230817-project-delta-50.jpeg)

**Data do último update:** 17/08/2023
**Maker:** Aregs (designed by Pao from Beatlab)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=121067.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 0     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **4** |

### O que aprecio

  * _Design_ cheio de pormenores para um _layout_ muito pouco comum, 50%.
    * Concorrente directo do TMO, mas que à partida parece ter melhor características.
  * Dois _weights_, sendo um apenas externo e de dimensões significativas e outro externo e interno.
  * Utilização de dois tipos de material nos _weights_, latão e aço inoxidável.
  * _Design_ da _plate_ sem _flex_ _cuts_.
  * O teclado está produzido, falta apenas definir onde e quando será vendido.

### O que não aprecio

  * Os _mounting_ _points_ estão distribuídos de maneira uniforme, mas a sua proximidade com a barra de espaços parece-me um pouco problemática. A rever.
  * Tendo em conta que os teclados já estão fabricados, era mais interessante que houvesse fotos destes em vez de renderizações.
  * Um IC que é apenas uma manobra de _marketing_ para dar a conhecer o projecto visto que a venda vai decorrer dentro de 15-20 dias.

### O que gostava de ver alterado

  * Apenas oferece _hotswap_ PCB, que tendo em conta o público alvo, não tenho a certeza se será a escolha certa.
  * Falta muita informação sobre as PCBs e _plates_.
  * Não há visibilidade sobre quais os _layouts_ suportados.
  * O preço é um pouco elevado, mas semelhante à concorrência. Com 50 unidades não se pode esperar muito melhor.
  * O _timing_ não é o melhor, visto que irá haver uma nova ronda de vendas do TMO, a terceira. Resta saber quem oferece o melhor produto e ficará a “ganhar” no final.
  

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=121147.0)

### PROS

  * _Design_ cheio de pormenores para um _layout_ muito pouco comum, 50%
    * Concorrente directo do TMO, mas que à partida parece ter melhor características.
  * Existência de _weights_ externos e internos.
  * Utilização de materiais de luxo nos _weights_, latão e aço inoxidável.
  * O kit base de venda incluí por defeito duas ofertas distintas de *plates*, uma em alumínio e outra em policarbonato. Algo interessante quando temos um _layout_ menos comum.
  * Venda em _stock_, ou seja, o teclado será enviado num espaço muito curto de tempo.

### CONS

  * Os _mounting_ _points_ não estão localizados na minha opinião nos melhores locais.
  * Apenas oferece uma PCB _hotswap_.
  * Não é partilhado de modo claro quais os _layouts_ suportados (penso que apenas suporta o _layout_ que apresenta nas fotos).
    * Ou seja, apenas suporta _split_ _spacebar_ e não suporta ISO.
  * Não utiliza uma USB _daughterboard_ _open_-_source_.
  * Não é disponibilizado um vendedor europeu, o que torna o seu custo mais elevado principalmente em Portugal.

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O Delta 50% é um projecto bastante interessante para aqueles que gostam de layouts mais pequenos. Neste caso, o layout utilizado é em muito semelhante ao TMO que é um teclado bastante popular nos 50%. Em termos de proposta de valor, com um preço muito semelhante ao TMO ($350), o Delta aparenta ter boa qualidade e algumas caracteristicas muito interessante como os dois weights em materiais distintos. No entanto, algumas opções (ex. os mounting points, ter apenas hotswap, etc.), sejam elas ao nível de design como ao nível de marketing, não deixam este ser um projecto de referencia. Gostaria ainda de realçar que esta venda é com unidades em stock, 50 unidades para ser mais preciso, o que significa que o tempo de espera para receber o teclado em questão não é elevado. No entanto, como também é habitual nestes projectos de nicho, não existe um vendedor europeu e como tal o seu custo será bastante mais elevado que os $350 propostos inicialmente (que por sinal acho que é um bom preço). Para quem procura um teclado 40%-50% e gostou do TMO, tem aqui a meu ver uma alternativa bastante válida.

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.