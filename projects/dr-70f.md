# DR-70F

![](assets/images/20230817-project-dr-70f.jpeg)

**Data do último update:** 17/08/2023
**Maker:** Daring Run

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=121073.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **6** |

### O que aprecio

  * Um _layout_ pouco comum, uma espécie de 70% com 5 teclas macro.
  * Uso de alumínio 6063.
  * Dois tipos de _mounting_, _gasket_ _mount_ e _sandwich_ _mount_.
    * Neste último usa umas peças de silicone para absorver melhor vibrações.
  * Permite inverter o _layout_ e usar como _southpaw_.
  * Disponibilização de uma versão com fios e sem fios.
  * Preço é bastante interessante ($180).
  * Os 3 cortes na parte inferior da _case_ para melhorar os aspectos sonoros, pode ser uma algo interessante.

### O que não aprecio

  * O facto de ter as aberturas na parte inferior, vai ser um ponto extra de entrada de “lixo” para dentro do teclado. Mas diria que é um mau menor.
  * _Design_ e construção à volta do uso de espuma.
  * IC form é apenas para recolher interesse na compra e não está focado em como poderá melhorar o produto. Tendo em conta que é um projecto que está numa fase inicial, não é muito interessante de se ver.

### O que gostava de ver alterado

  * Não me parece que use uma USB _daughterboard_ _open-source_, mas carece de confirmação.
  * Apesar de ser apenas uma escolha estética, o uso de uma peça em pele na parte inferior do teclado, vai ser sujeita a grande desgaste e é provável que fique destruída ao fim de alguma utilização.
  * Gostava de ver mais informação sobre a PCB.
  * Não é 100% claro se o teclado terá _weights_ na parte interior ou não.

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.