# Duality TKL

![](assets/images/20230531-project-duality-f13-tkl.jpeg)

**Data do último update:** 21/06/2023
**Maker:** @streamline (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120331.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 0     |
| Layouts suportados   | 0     |
| Preço Estimado       | 1     |
| Bónus                | 1     |
| **Total**            | **5+1** |

### O que aprecio

- A variedade de possibilidades de mounting que o teclado permite (_plateless_, _baby_!).
- Possibilidade WK e WKL num _layout_ TKL.
- A altura na frente do teclado parece ser ligeiramente mais baixo do que o costume, ao nível de teclados como o Geonworks F1 722.
- Adopção do novo conector _standard_ para as _open-source_ USB _daughterboard_.
- Suporte para uma barra de espaços com tamanho 10u.
- Ter como fabricante a Dadesin, a qual é uma das melhores fabricas com conhecimento sobre teclados na China.
- Apesar do preço elevado, a abertura que existe por parte do criador em partilhar os custos de fabrico das diversas partes (ponto bónus).
- Muitas fotos dos protótipos que mostram bastante qualidade.

### O que não aprecio

- O preço é demasiado elevado para o que oferece na minha opinião. Observei os custos e parece-me que estão sendo considerados valores demasiado elevados para os acessórios. Na minha opinião o criador poderia baixar o preço final 40-50€. A margem do criador parece ser na casa dos 20%, o que é aceitável num projecto deste tipo, mas não é uma referência.

### O que gostava de ver alterado

- Gostava de ver mais informação sobre o projecto, que considero importante: informação sobre as _plates_, PCBs, _layouts_ suportados, etc.
- Gostava de perceber melhor como é feito o _mounting_. O criador fala em diversos _mountings_ disponíveis e gostava de ver mais informação sobre estes.
  

## Group Buy

Link: [Vendedor Internacional](https://prestigekeys.com.au/collections/duality), [Discord](https://discord.com/channels/876599053322952755/1084117135279796284/1118899268254707862), [Geekhack](https://geekhack.org/index.php?topic=120598.0)

Review do IC [aqui](https://perifericosdeescrita.substack.com/i/125061045/duality-tkl).

### PROS

  * Variedade de possibilidades no _mounting_.
  * As cores disponibilizadas são bastante fora do comum para um TKL (ver cor Pazuzu).
  * Permitir _plateless_.
  * Design com vários _weights_ em materiais como alumínio 6063, cobre ou aço inoxidável.
  * Abertura do criador sobre todo o processo, indicando custos, fabricante, etc.
  * Ser fabricado numa das melhores fábricas com conhecimento sobre teclados na China.

### CONS

  * A imagem actual do criador não é a melhor, devido ao insucesso do projecto Cor65XT.
  * Para além do ponto anterior, este GB vai ser gerido apenas em colaboração com um revendedor/distribuidor, estará a decorrer até Setembro e não tem limite de unidades.
    * Em termos de logística, parece-me bastante arriscado.
  * Para se aproveitar os diversos tipos de _mounting_ oferecidos, é necessário comprar duas versões da _plate_.
  * Não haver por defeito plates com suporte ISO.
  * Ter desistido da ideia de suportar o novo conector das USB _daughterboard_ _open-source_. Apesar de compreender as razões do criador (não ficou satisfeito com a durabilidade do conector nos protótipos), porque não suportar os dois conectores?
  * Não ter um revendedor ou distribuidor na EU o que torna o preço final deste teclado demasiado elevado. O preço base com portes, uma plate e uma PCB é ligeiramente acima dos 600€. Se adicionarmos importação o preço torna-se perto dos 800€.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [x] Não recomendo

**Comentário:** O Duality TKL é um teclado com um _design_ e uma temática associada bastante diferenciada para um TKL. É um projecto cheio de boas intenções e que se nota que o criador dedicou muitas horas a refinar. Infelizmente, apesar de no papel parecer muito interessante, tenho que alertar para o risco associado a este projecto. Este é o segundo projecto do criador, e infelizmente o primeiro projecto não teve o final pretendido. Apesar de na minha opinião o criador não ter tido grande parte da responsabilidade, a verdade é que o Cor65XT não foi um sucesso e encontra-se ainda de momento em processo de devolução do dinheiro a todos os compradores. Apesar disso, o criador lança-se num GB ilimitado, com um projecto ambicioso ao nível de opções permitidas, com custo elevado e desta vez praticamente a solo e apenas com um revendedor/distribuidor associado. Um projecto desta dimensão, se por alguma razão tiver um pequeno problema, tem uma grande probabilidade de não se concretizar (veja-se o que aconteceu com o Geonworks F2-84) e algumas pessoas poderem ficar lesadas financeiramente. Pessoalmente preferia que o criador tivesse limitado o número de opções e unidades nesta primeira ronda de vendas, e depois com mais experiência e confiança, voltar a fazer novas rondas de venda. Infelizmente não foi esse o caminho seguido, e apesar de gostar do projecto, não posso recomendar a compra.

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.