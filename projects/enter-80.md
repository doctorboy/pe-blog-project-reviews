# Enter80

![](assets/images/20230613-project-enter-80.jpeg)

**Data do último update:** 13/06/2023
**Maker:** Kezewa Studio

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120480.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 0     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **4** |

### O que aprecio

  * *Design* simples, e que me parece inspirado no _Derivative_, mas em _layout_ TKL.
  * Oferta extensa de cores.
  * Alumínio 6063.
  * Um pequeno _weight_ interno e externo.
  * O _weight_ a utilizar um material _premium_ que está na moda, cobre.
  * Mala incluída.
  * Utilização de uma USB _daughterboard_ que é _open source_.
  * Suporte de vários _layouts_, incluindo ISO.
  * Preço base parece ser competitivo, na casa dos $200 (sem impostos).

### O que não aprecio

  * Não tem IC _form_ para recolher _feedback_. Este está a ser feito de maneira não estruturada no Geekhack.
  * Configuração base recorrendo a espuma, quando ainda não existe um protótipo.
  * O _mounting_ em _top mount_ terá um ponto de _mounting_ mesmo na zona da barra de espaços, algo que não é de todo recomendado.
  * Não consigo entender a referência de incluir plate _stabs_, quando me parece que a plate parece suportar apenas PCB _stabs_. O criador confirma que PCB _stabs_ são suportados.
  * Não são referenciados distribuidores, como tal para terras lusas deve requerer importação.

### O que gostava de ver alterado

  * Gostava que o projecto tivesse protótipos, principalmente quando existe ambição de realizar 13 cores diferentes.
  * Tem dois tipos de _mounting_, no entanto, apenas um deles é utilizável.
  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.