# Frost TKL

![](assets/images/20230725-project-frost-tkl.jpeg)

**Data do último update:** 25/07/2023
**Maker:** @keypport (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120922.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **7** |

### O que aprecio

  * Dois _layouts_ disponíveis, WKL e WK.
  * _Weight_ em aço inoxidável
  * O _design_ da peça inferior que tem "sulcos" interiores de modo a diminuir ao máximo o espaço vazio dentro da _case_.
  * A existência de vários protótipos em diversas cores.
  * Gosto da ideia de não se usar espuma no interior da _case_.
  * O _design_ da plate parece-me aceitável, apesar de ter alguns _cutouts_ que dispensaria.
  * Suporte de múltiplos _layouts_, incluindo ISO.
  * Utilização de uma PCB bastante popular e robusta como a h87nu do [Hiney](https://hineybush.com/).
  * Utilização de uma USB _daughterboard_ _open_ _source_.
  * O preço parece aceitável tendo em conta o MOQ de 50 unidades.

### O que não aprecio

  * Pessoalmente não aprecio a escolha de se utilizar pequenos _washers_ em espuma na parte interior da _case_, que segundo o criador servem para eliminar as vibrações da _plate_ e PCB.
    * Na minha óptica, se a ideia é eliminar as vibrações da _plate_ e PCB, então deve-se isolar estas peças da _case_. Os discos no fundo, apenas vão introduzir novos pontos de contacto entre a PCB e a case, que não são espacialmente consistentes e vão introduzir variações na flexibilidade da PCB. As vibrações irão continuar a propagar-se para a case através dos _mounting_ _points_.
    * No meu ponto de vista, estes _washers_ em espuma servem essencialmente para evitar que a PCB toque na case e haja algum tipo de curto-circuito.
    * Isto é apenas a minha opinião sem testar o teclado, posso estar totalmente errado.
  * A pequena dimensão do _weight_.
  * A localização do _badge_ (opinião puramente pessoal).

### O que gostava de ver alterado

  * Actualmente acho que faria mais sentido usar alumínio 6063 em vez de 6061 (a diferença de preço deverá ser pouco significativa).
  * O _weight_ ser apenas externo, quando haveria a possibilidade de ter alguma exposição interna.
  * Pelas fotos partilhadas o acabamento da anodização parecer não ter a qualidade de outros teclados (o mesmo é indicado no [vídeo do Alexotos](https://www.youtube.com/watch?v=tKfaqV1hiTU)). No entanto, estes são apenas protótipos.
  * Ser disponibilizado um vendedor na Europa.
  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.