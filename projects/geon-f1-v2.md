# Geonworks F1-8X v2
A reedição de um clássico.

![](assets/images/20230606-project-geon-f1-v2.jpeg)

**Data do último update:** 6/06/2023
**Maker:** Geonworks

## Interest Check

IC realizado antes desta iniciativa.  

## Group Buy

Link: [Anuncio no Discord](https://discord.com/channels/743801649377574924/984755779741556757/1114085977951059988)

### PROS

- Nova versão com vários pequenos melhoramentos do famoso e excelente Geonworks F1-8X.
- A utilização de novos materiais nobres que estão de momento em moda, neste caso o cobre.
- O suporte de USB _daughterboard_, importante principalmente num teclado com um posicionamento tão alto da porta USB e onde pode haver alguns acidentes.
- O melhoramento de agora ter um pequeno sulco para acomodar melhor o o-ring na montagem.
- Possibilidade de utilizar várias PCBs universais, visto que suporta PCBs com porta USB e PCBs que utilizam USB _daughterboards_.
- Produto de um dos melhores criadores da actualidade no _hobby_.
- A versão original é considerada pela generalidade das pessoas no hobby como um teclado de referência no formato TKL, com óptima sensação de escrita e características sonoras (pessoalmente também subscrevo).
- Este é apenas a primeira venda de muitas. As cores irão variar ao longo do tempo.

### CONS

- Não tem vendedor/distribuidor na europa o que torna o seu preço muito elevado.
- Apesar do novo design, um dos principais pontos fracos na minha opinião ainda se mantém, o posicionamento demasiado alto da porta USB.
- Utilização de uma USB _daughterboard_ que tem design próprio. Poderia ser sido adoptada uma versão open source.
- A necessidade de ter alguns _cutouts_ no weight superior direito.

### Recomendo comprar?[^1]

- [x] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** Como proprietário de um Geonworks F1-8X 722 que pessoalmente adoro, estou numa posição confortável para facilmente recomendar esta nova versão de um dos melhores clássicos no hobby dos teclados mecânicos, o F1-8X. No entanto, apesar do seu preço base fantástico, não posso fugir ao tópico de que esta compra obriga a importação. Em território nacional, significa transformar um produto com custo base na casa dos 400-450€ para valores superiores a 600€ e até bastante perto dos 700€, dependendo dos extras escolhidos. Este preço é um valor que definitivamente não é para todas as carteiras. Se o valor final não é problema, então este é provavelmente um dos melhores TKLs que se pode comprar nos dias actuais. Tem uma identidade ao nível de design bastante interessante, a qualidade dos materiais utilizados é muito boa, o criador é das pessoas mais entendidas no hobby, todo o teclado está cheio de pequenos pormenores deliciosos e o resultado final é bastante bom. No entanto, são mais de 600€ por um produto vendido no outro lado do mundo a menos de 400€ (e que vai vender muito, se gosta de produtos raros, este não é um produto a considerar), e isso mentalmente pode ser difícil de digerir. Existem alguns rumores que no futuro talvez haja a possibilidade de o Geon ter uma parceria na Europa para a distribuição dos seus teclados, mas de momento são apenas rumores. A oferta inicial de cores é muito limitada, mas mais cores virão, se não gostar desta cor, só tem que esperar mais uns meses, pois como disse, irão haver no futuro mais vendas deste magnifico teclado.

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.