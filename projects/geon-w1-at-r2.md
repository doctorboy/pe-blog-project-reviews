# W1-AT R2

![](assets/images/20230715-project-geon-w1-at-r2.jpeg)

**Data do último update:** 15/07/2023
**Maker:** Geonworks

## Interest Check

IC realizado antes desta iniciativa.  

## Group Buy

Link: [Página Oficial](https://geon.works/collections/w1-at), [Fotos do Markerchun (R1)](https://markerchun.com/geonworks-w1-at)

### PROS

  * _Layout_ pouco convencional, que no início estranha-se e depois entranha-se.
    * Teclas macro na esquerda, 60% no meio e _numpad_ à direita.
  * Utilização de materiais premium, como alumínio 6063, e latão no _weight_.
  * Um _weight_ externo e interno.
  * _Mount_ _tadpole_ _plateless_ como base e onde depois é possível adicionar _plates_ para obter um _feeling_ diferente.
    * _Plates_ disponíveis em materiais variados como _add-on_.
  * _Layout_ ISO disponível com a utilização de _solderable_ PCBs.
  * Utilização de uma USB _daughterboard_ modelo _open_ _source_.
  * Utilização de _bumpons_ na parte inferior de boa qualidade.

### CONS

  * Não disponibilizar um vendedor europeu, que prejudica bastante a proposta de valor deste excelente teclado.
  * As _plates_ não suportam por defeito o layout ISO.
  * Disponibilizar por defeito apenas um conjunto com uma dureza fixa nos _tadpoles_. Seria mais interessante disponibilizar mais opções e quiçá até incluir 2-3 conjuntos por defeito. O custo acrescentado seria baixo e permitiria um bom nível de personalização do utilizador.
  * O preço base ($485) parece ser pouco competitivo para um teclado que consiste em ser um [Frog Mini](https://geon.works/collections/frog-mini) (~$240) com _numpad_ e duas colunas de _macro keys_. No entanto, o _layout_ é bastante único e certamente que o MOQ não é muito elevado, como na linha do Frog.
  * Não é propriamente um contra, mas de notar que nesta venda apenas duas cores estão disponíveis.
  * O peso de “apenas” 2,3 Kg para um teclado desta dimensão, ainda para mais do Geon, desilude um pouco (em termos de comparação, o F1-8X 722, que é um TKL, pesa 3,2 Kg desmontado). Nota-se aqui que é um derivado do design do Frog, sendo um teclado mais _budget_.
  * Sem nunca ter experimentado, ter 14 _mounting_ _points_ parece-me exagerado, mesmo para um _layout_ desta dimensão.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** Nova ronda de vendas do W1-AT (a ocorrer no final deste mês de Julho), um teclado com um _layout_ muito próprio e que pessoalmente acho que tem bastante potencial. Todos os utilizadores de 60%, que tenham a necessidade de utilizar um _numpad_, têm aqui a possibilidade de ter um teclado com nível alto de qualidade que satisfaça as suas necessidades. Em termos de _design_ e _mounting_, o W1-AT é muito inspirado no Frog da Geonworks, o qual é globalmente reconhecido como uma referência nos TKLs, trazendo logo óptimos indicadores. Pessoalmente, o meu grande “problema” com o W1-AT é mesmo o preço. Não acho que seja um preço competitivo por base, apesar de ser aceitável, mas o simples facto de requerer importação para o mercado europeu, torna o preço final acima dos 650€. Por este preço, entramos na gama de preços de teclados _premium_, e como tal esperaria um “F1-8X” com _numpad_ e a pesar 3,5 Kg. Se o preço não é um problema para ti, e achas o layout interessante (este é o grande factor de diferenciação do W1-AT), avança, pois não deixa de ser um excelente teclado com bons níveis de qualidade.


[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.