# Glare 65

![](assets/images/20230715-project-glare-65.jpeg)

**Data do último update:** 15/07/2023
**Maker:** Glare

## Interest Check

IC realizado antes desta iniciativa.    

## Group Buy

Link: [GB page](https://geon.works/collections/glare65), [Fotos do Markerchun](https://markerchun.com/glare65), [Vídeo do Markerchun](https://www.youtube.com/watch?v=OTgw___Qzo0)

### PROS

  * Design bastante bonito na minha opinião, com HG _sides _(invertidos?) e um _weight_ externo de dimensões bastante grandes.
  * O facto do criador ter ouvido o feedback sobre o Glare TKL e ter refinado ainda mais um já excelente produto.
  * O peso absurdo, que não deixa ninguém indiferente. Será este o 65% mais pesado do mercado?
    * Aqui não há lugar para espuma, apenas para materiais premium.
  * A utilização dos novos conectores molex na PCB compatíveis com as USB _daughterboard_ _open-source_.
  * Utilização de 3 materiais _premium_, uma peça de topo em alumínio 6063, uma peça inferior em latão e um weight em aço inoxidável.
  * _Plates_ com _cutouts_ para os _stabs_ de maiores dimensões para que se possa ajustar os mesmos sem desoldar o teclado na totalidade.
    * De referir e louvar a ausência de _meme cuts_.
  * Design visual da PCB bastante interessante.
  * O preço, para o que o teclado apresenta, é difícil de superar.

### CONS

  * Não suporta ISO (porquê Glare, porquê?).
  * Não disponibilizar um vendedor europeu, que prejudica bastante a proposta de valor deste excelente teclado.
  * A USB _daughterboard_ não ser um modelo _open_ _source_.

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O Glare 65 é um 65% que segue as linhas muito próprias do seu irmão mais velho, Glare TKL, que foi lançado 2022. Este é o segundo teclado idealizado pelo criador Glare, com o suporte da Geonworks, autor de outros teclados de renome como o [F1-8X](https://perifericosdeescrita.substack.com/i/126412143/geonworks-f-x-v) ou a linha Frog (layouts TKL e 60%). O seu criador é uma pessoa envolvida há vários anos no hobby e já com vários teclados criados no passado, como tal, é expectável que o Glare 65 se torne uma das referências na sua área nos próximos anos. Ao nível de design, pessoalmente acho-o muito bonito, e esta versão 65% já vai receber de base algumas melhorias sugeridas pela comunidade aquando do lançamento do Glare TKL, como, por exemplo, a adoção de uma peça inferior em latão, com o objectivo de melhorar a acústica e de aumentar o seu peso bruto (~3.3Kg desmontado). Não tenho grandes dúvidas que o Glare 65 será um dos teclados mais icónicos do ano, e satisfará os seus compradores com uma experiência de utilização bastante agradável. Como principal ponto negativo, destaco o não suportar o layout ISO, em nenhuma das opções. Já sei que o Glare não é fã do layout ISO, mas acrescentar o seu suporte na PCB e disponibilizar ficheiros de plates para ISO, é um esforço mínimo. É incompreensível para mim, como num teclado deste nível o suporte para ISO não é disponibilizado. Destaco também pela negativa, a não utilização de uma USB _daughterboard_ _open_ _source_, e no seu lugar a utilização, expectável diga-se, da proposta da Geonworks. Antes de irmos ao preço, gostava também de referir, que os 11° de ângulo que o Glare 65 apresenta (já era assim com o TKL), não é para todas as pessoas e poderá representar um desafio para alguns utilizadores. Relativamente ao preço, se tivermos em conta o design global do teclado, os materiais utilizados e o seu peso, trata-se de um preço bastante competitivo. Com um preço base de $500, este é em muito semelhante ao do seu irmão mais velho, que foi $520 e que se por um lado tinha dimensões bastante superiores, por outro, não tinha a peça inferior em latão. Mesmo assim, pessoalmente, acho que em termos de proposta de valor, o seu concorrente [Orbit 65](https://perifericosdeescrita.substack.com/i/125061045/orbit) é uma melhor proposta, ao apresentar um design de perfil curvo, uma parte inferior na sua totalidade em cobre e um preço aprox. 50€ mais baixo. Ah, e suporta ISO! No entanto, tal como o Orbit, a aquisição de um Glare 65 irá implicar importação em território europeu, e torna o custo deste teclado um pouco desajustado. O preço base, juntamente com portes e custos de importação, facilmente vai rondar os $650-$700, um valor bastante elevado para um 65%. Mas como o Glare não se preocupou muito em adicionar suporte ISO, acredito que ele infelizmente também não estará muito preocupado com o mercado europeu. Se dinheiro não é problema, 65% é o seu _layout_ de eleição, não gosta de usar ISO e não adquiriu o [Orbit 65](https://perifericosdeescrita.substack.com/i/125061045/orbit), dificilmente encontrará uma melhor proposta este ano que o Glare 65 (GB já amanhã).

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.