# Type K

![](assets/images/20230825-project-gok-type-k.jpeg)

**Data do último update:** 25/08/2023
**Maker:** Gok

## Interest Check

IC realizado antes desta iniciativa.  

## Group Buy

Link: [Geekhack IC](https://geekhack.org/index.php?topic=107031.0), Oblotzky Industries [Pag. 1](https://oblotzky.industries/products/type-k-anodized-black), [Pag.2 ](https://oblotzky.industries/products/type-k-anodized-silver), [Pag.3](https://oblotzky.industries/products/type-k-powder-beige), [Página oficial](https://www.gok.design/type-k)

### PROS

  * Um _design_ bastante particular, que foi pioneiro quando apresentado em 2020. Após 3 anos de desenvolvimento e 7 rondas de protótipos, é bom ver finalmente este projecto tornar-se realidade.
  * O projecto é desenvolvido por um criador com renome no _hobby_, o Gok.
    * A parte eletrónica foi também desenvolvida por outro criador de PCBs de renome, o Gondo.
  * A estética deste teclado é muito particular e interessante.
    * O teclado apresenta um grau de inclinação lateral de 7º para cada uma das mãos e apresenta uma estética focada na simetria.
  * Disponível com _solderable_ PCBs e PCBs _hotswap_.
  * Utiliza um _mount_ designado por “_grommet_ _mount_”, que consiste em ter umas peças de silicone (que podem ter diferentes tipos de dureza) na *plate* e que fazem o contacto com a *case* do teclado.
  * Suporte de QMK/VIA.
  * Utilização de um _weight_ externo em aço inoxidável.
  * A parte inferior ser toda em policarbonato irá dar alguma personalidade própria ao nível sonoro.
  * A inclusão por defeito do apoio para os pulsos, que acho fundamental para este teclado em particular, quer seja por questões estéticas como de utilização.
  * Futuro suporte de PCBs para _switches_ electrocapacitivos, que estão em desenvolvimento com o suporte do Gondo.
  * Disponível num dos melhores vendedores Europeus (no entanto, realçar que toda a comunicação sobre a venda do Type-K pela Oblotzky Industries tem sido muito fraca e desapontante desde início).

### CONS

  * O preço, apesar de o compreender em certo modo (i.e. anos de desenvolvimento, dezenas de protótipos, etc.), é muito elevado para o que é oferecido.
  * Não utiliza uma USB _daughterboard_ _open_-_source_. Percebo o porquê, mas para um teclado que é publicitado como focado na melhor experiência de utilização, são pequenas coisas que ficaram um pouco esquecidas.
  * O _weight_ tem marcas de maquinação na parte interior. Esteticamente é algo que não se vê, mas é um bocado estranho estas existirem num teclado que se espera que seja tudo com a melhor qualidade possível.
  * Os _leds_ RGB na parte inferior do teclado pouco ou nada acrescentam visualmente e acho que não foram aproveitados da melhor forma.
  * Necessita de _stabs_/teclas com a dimensão 3u para a barra de espaços, algo pouco comum no _hobby_ (suporta os tamanhos 2.25u e 2.75u também e os _stabs_ vêm incluídos no kit, o que atenua parte do problema).
  * Tal como é muitas vezes hábito em teclados ergonómicos, o som produzido pelo teclado tem alguns aspectos menos bons que precisam de ser compensados e ajustados aquando da montagem (ex. no stream do Taeha é notório que a utilização de _force_ _mod_ ajuda a diminuir alguma da ressonância presente).

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** Penso que posso dizer confortavelmente que o Type-K é o teclado mais antecipado no _hobby_ nos últimos 1-2 anos. Quando este foi apresentado em Junho de 2020, criou desde cedo muito interesse devido essencialmente a dois factores: a sua estética e ergonomia muito particular que tenta melhorar a experiência de utilização e o facto também de ser um teclado de um criador muito popular no _hobby_, o Gok. No entanto, o panorama global económico mudou bastante e as vendas em projectos mais de nicho, como o Type-K têm sido bastante prejudicadas. Como tal, havia algumas dúvidas se este projecto alguma vez iria ver a luz do dia. Após 3 anos de desenvolvimento, dezenas de protótipos (foram 7 rondas com melhoramentos), e muita dedicação em tentar entregar um produto com a melhor qualidade possível, o Type-K está finalmente aí, disponível em _Group_ _Buy_ ilimitado, mas infelizmente com um preço proibitivo (já se esperava). Pessoalmente, tendo em conta o panorama global, acho que é um pouco desapontante que não tivesse havido um aviso mais atempado do criador para toda a comunidade a indicar que o lançamento do Type-K estaria para breve (tipo com uma antecedência de 3-4 meses), com um indicador do preço alvo. Deste modo, todas as pessoas que estivessem interessadas poderiam começar a preparar-se para esse evento (ex. poderiam vender alguns dos seus teclados, poupar dinheiro, etc.). Ao invés, o Type-K foi anunciado apenas com uma antecedência aproximada de 1 mês e o período de venda são apenas 2 semanas, isto quando o preço base deste menino é de $750 configuração base já com apoio para os pulsos. Se ignorarmos o preço, o Type-K será certamente um teclado de referência no _hobby_, principalmente no mercado dos ergonómicos. Está longe de ser um teclado perfeito, e chega mesmo em um ou outro aspecto a desapontar um pouco tendo em conta todo o trabalho feito (ex. som produzido). No entanto, não tenho dúvidas que a experiência de utilização será superior a qualquer outro ergonómico e na minha opinião, esteticamente é uma verdadeira obra de arte. No mercado europeu, será vendido a 850€ (preço para Portugal) pela Oblotzky Industries (de realçar que a Oblotzky Industries lançou a venda muito mais tarde que todos os restantes vendedores e desde inicio teve uma comunicação com os clientes demasiado amadora, principalmente quando este vendedor é considerado como “o melhor da europa”). O preço é difícil de justificar nos dias actuais, principalmente para um teclado que pesa aproximadamente 2.5Kg e não utiliza _weights_ de grandes dimensões. Se és um verdadeiro apaixonado pelo _hobby_ que está envolvido há vários anos, já tens vários outros teclados de boa qualidade e tens capacidade financeira para despender aproximadamente 1000€ num teclado, o Type-K é uma compra válida. Se não te enquadras neste perfil, diria que o Type-K não faz muito sentido para ti e é muito difícil de justificar. Diria que o melhor é aguardar, pois acredito que no espaço de 1-2 anos, é possível que um _maker_ como a TKD ou a QK decida criar um teclado muito semelhante por metade do preço (tal como aconteceu no passado com outras referências como o TGR Alice). Até lá, o Type-K certamente que irá fazer muito furor por essas _meetups_ no mundo inteiro e irá para sempre tomar o seu lugar na história ao pé de outras referências como o Singa Unikorn ou TGR Alice.


[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.