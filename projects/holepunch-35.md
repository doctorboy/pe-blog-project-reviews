# Holepunch35

![](assets/images/20230802-project-holepunch-35.jpeg)

**Data do último update:** 02/08/2023
**Maker:** Holepunch Studios

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120968.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 0.5     |
| Protótipos           | 0     |
| Mounting visível     | 0     |
| Layouts suportados   | 0     |
| Preço Estimado       | 0     |
| Bónus                | 1     |
| **Total**            | **3.5** |

### O que aprecio

  * Este é provavelmente um dos teclados com _layout_ inferior a 50% mais bonitos que vi nos últimos tempos.
  * Um dos melhores IC _forms_ que tenho visto nos últimos tempos, com perguntas e opções claras. Ponto extra.
  * Utiliza a versão mais actualizada de uma USB _daughterboard_ _open_ _source_.

### O que não aprecio

  * É um projecto ainda numa fase muito inicial.
    * Conceito engraçado, mas de viabilidade reduzida no mercado actual. De louvar, no entanto, o interesse em partilhar este tipo de projectos.
    * Na minha opinião, um projecto deste género devia ter um pouco mais de maturidade antes de ir para IC no Geekhack.

### O que gostava de ver alterado

  * Não dá para fazer o mesmo design, mas em Alice normal?
  * Ver toda a informação que falta, que ainda é bastante: como é o mounting, partes internas, PCBs, etc.

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.