# Join 65%
A budget-friendly 65%

![[assets/images/20230512-project-join-65.jpeg]]

**Data do último update:** 12/05/2023
**Maker:** KnifeLab, distribuido pela Thocc Supply

## Interest Check

IC realizado antes desta iniciativa.

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120283.0)

### PROS

  * O preço é interessante para um 65% em alumínio. Para a zona europeia o preço base são 170€, enviado pela KeyGem na Alemanha.
  * Ter peças que permitam ser trocadas para mudar a cor, como a parte lateral do teclado.
  * Não gosto de espuma por defeito, mas neste caso acho que será um teclado que irá beneficiar de ter espuma.
  * PCB sem _flex cuts_, com 1.2mm de espessura, parece ser uma boa opção.
  * _Plates_ a preços aceitáveis.

### CONS

  * O facto de ser composto por muitas peças, levanta desafios ao nível de som que não sei se foram endereçados.
    * No entanto, a espuma deve ajudar nesse aspecto.
  * PCB _hotswap_ não suportar ISO.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O Join65 parece ser um teclado _budget_ interessante, dentro do que tem sido realizado por outros criadores. O factor diferenciador, a meu ver, é a capacidade de personalizar a cor das laterais do teclado. O preço também parece ser equilibrado. Quem gostar do design e esteja agora a iniciar-se no _hobby_ ou simplesmente à procura de um primeiro teclado personalizado, é uma opção aceitável.


[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.