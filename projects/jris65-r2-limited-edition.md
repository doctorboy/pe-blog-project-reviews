# JRIS 65 R2 Limited Edition
 

![](/assets/images/20230509-project-jris-65-r2.jpeg)

**Data do último update:** 31/05/2023
**Maker:** IRISlab

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120224.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 0     |
| Bónus                | 0     |
| **Total**            | **6** | 

### O que aprecio

- Que estejam a tentar melhorar uma *board* já existente, em vez de fazer *reruns* da versão existente.
- Terem adicionado mais cores para a *case* e *weights*.
  
### O que não aprecio

- Falarem em alterações que não mostram o resultado final, ex. Non Flex PCB, peça de *foam* alterada, etc.
- Para um R2 podiam ter focado em melhorar o teclado para não necessitar de espuma no seu interior.
- Depois de um R1 e já com protótipos, é fundamental partilhar o preço esperado.
- Recomendar mais um nível de *foam* para a versão *plateless*.

### O que gostava de ver alterado

- Plates sem *meme-cuts*
- A localização do connector JST na PCB não está muito bem posicionado, pois interrompe um *flex cut* a meio dos alphas que potencialmente cria uma variação no *typing feel* nas teclas *alpha*.
- Imagens sobre o *mounting* estão misturadas no meio das imagens dos protótipos. Deveriam estar separadas.
- Com o *mounting* deste teclado (gaskets via PCB), para mim apenas faz sentido usá-lo com uma solder PCB e *plateless*. Preferia que levassem as pessoas a usá-lo dessa maneira, em vez de o *default* ser *hotswap* com *plates*.

## Group Buy

Link: [Offical page](https://www.mykeyclub.com/pages/jris65-r2-groupbuy), [Geekhack](https://geekhack.org/index.php?topic=120413.0)

### PROS

- Ainda não é possível ver com 100% de certeza qual o preço final deste GB, porque o vendedor europeu não criou ainda a página do produto (e estamos no dia de lançamento do GB). Contudo, a minha expectativa é que o preço seja muito semelhante a teclados concorrentes, como o [Cub65](https://perifericosdeescrita.substack.com/i/123574171/cub). Se assim se confirmar, o Jris 65 R2 apresenta um preço muito competitivo para o que oferece.
- Tem a opção de ter um _weight_ em aço inoxidável, o que é fantástico para o preço praticado e para um teclado de gama de entrada como este.
- Na _review_ do Alexotos, a versão R1 apresentou uma boa qualidade no acabamento. Tudo leva a crer que a versão R2 será na mesma linha.
- Oferta ao nível da PCB é bastante completa, com suporte ao _layout_ ISO mesmo na versão _hotswap_. Versão _solderable_ suporta ainda o _layout_ com a barra de espaços 7u.
    - Tem uma oferta com PCB _bluetooth_, mas como não suporta software _open-source_, pessoalmente não recomendo.
- A versão _hotswap_ da PCB está disponível sem _flex cuts_. Tal como esperado, o Alexotos na _review_ do R1 mencionou que para quem usa _plate_, os _flex cuts_ não adicionam nada ao teclado, provavelmente até terá um impacto negativo na parte sonora.
- Opção para montar _plateless._
- Muita oferta ao nível de cores.

### CONS

- A versão sem fios, que usa _bluetooth_, não usa _firmware_ _open-source_.
- A versão _solderable_ da PCB não está disponível sem of _flex cuts_.
- Ao nível sonoro, com a espuma no seu interior, tem um perfil sonoro típico de teclados com espuma. Fica a dúvida como se comporta sem esta, sendo que a versão R1 portou-se até bastante bem na review do Alexotos.
- A venda na Europa será realizada pela Candykeys, que infelizmente não é um dos melhores distribuidores de momento. O apoio ao cliente costuma ser lento e ineficaz e os envios demorados, principalmente com teclados que tiveram muitas vendas.

### Recomendo comprar?[^1]

- [x] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** O Jris 65 R2 é para mim, neste momento, o melhor teclado de gama de entrada que se pode comprar. O preço é competitivo e oferece determinadas características que a concorrência nem sempre oferece, como, por exemplo, PCB _hotswap_ com _support_ para ISO e a opção de ter um _weight_ em aço inoxidável com acabamento PVD. Talvez a grande desvantagem deste teclado seja mesmo o seu distribuidor na Europa, que poderá trazer alguns problemas. Se estás à procura do teu primeiro teclado mecânico com alguma qualidade, e estiveres pronto para alguns problemas de logística, recomendo o Jris 65 R2.

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.