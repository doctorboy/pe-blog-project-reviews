# KBDfans Pluto

![](assets/images/20230627-project-kbdfans-pluto.jpeg)

**Data do último update:** 27/06/2023
**Maker:** Desenhado pelo Saab, produzido pela KBDfans

## Interest Check

IC realizado antes desta iniciativa.    

## Group Buy

Link: [Página Oficial](https://kbdfans.com/products/pluto?variant=41473592295563), [IC no Geekhack](https://geekhack.org/index.php?topic=119343.0)

### PROS

  * Mais um projecto FRL, desta vez da popular KBDfans.
  * Opção de escolher entre dois layouts, WK e WKL.
  * Em termos de _design_, adopta um _seamless_ HG _side_ que eu pessoalmente gosto bastante.
  * PCBs sem _flex cuts_ e disponível com 1.2mm e 1.6mm.
  * Estão disponíveis duas versões *hotswap* da PCB, uma com suporte a ISO e outra a ANSI.
    * Temos, portanto, suporte de ISO quer na versão _solder_ como na versão *hotswap*.
  * Dois _mounting types_, _top_ _mount_ e _O-ring_
  * A existência de um _weight_ interno e como opção ter a parte inferior toda em latão.
  * _Plate_ sem _meme cuts_.
  * Utilização de alumínio 6063.
  * Ter o USB _daughterboard_ parcialmente acomodada com o _weight_ interno, mostra atenção ao detalhe.
  * O preço parece ser muito interessante para o que é oferecido (menos de $230), ainda para mais tendo em conta que é uma _pre-order_. Não consegui perceber qual o preço da versão com o fundo em latão, sendo a proposta no papel mais interessante do teclado.

### CONS

  * Não serem apresentadas fotos de protótipos na página do produto.
  * Não ter revendedor ou distribuidor na Europa (pelo menos não é falado na página do produto).
  * O peso de 1.2kg (não montado) parece-me baixo para as dimensões do teclado. O que me leva a concluir que houve aqui algum corte no material utilizado.
  * Gostava de ver mais informação sobre a USB _daughterboard_. Parece ser utilizado o modelo _open source_ mas não tenho certeza.
  * Pessoalmente não sou apreciador da marca KBDfans, visto que estes têm um passado conflituoso com alguns criadores conceituados no _hobby_ dos teclados mecânicos.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O KBDfans Pluto é, sem dúvida, um dos melhores projetos que vi sair da KBDfans nos últimos anos. O teclado apresenta surpreendentemente vários detalhes que demonstram muita atenção ao detalhe por parte da KBDfans. Em termos de opções, foram seguidas praticamente todas as principais recomendações, evitando-se tendências passageiras apenas por uma questão de moda (como, por exemplo, os cortes "meme" nas *plates*). No entanto, existem dois pontos que me levam a não recomendar este teclado de olhos fechados: a falta de vendedores na Europa, o que prejudica a competitividade do preço, e o histórico de atitudes da KBDfans que não estão alinhadas com os meus princípios no hobby, o que torna um pouco mais difícil para mim recomendar e comprar os seus produtos. Se não tens problemas com a marca KBDfans e não te importas de pagar um valor extra aproximado de 30-35% por um produto importado, o Pluto aparenta ser um produto consistente e uma boa proposta no mundo dos FRL. No entanto, acho que existem melhores ofertas como a [TKD Cycle7](tkd-cycle7.md) (em breve irá estar em GB) e o [Violetta 70](violetta-70.md).

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.