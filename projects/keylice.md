# Keylice

![](assets/images/20230703-project-keylice.jpeg)

**Data do último update:** 03/07/2023
**Maker:** Kenny42 Studio

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120712.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 0.5   |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **6.5** |

### O que aprecio

  * Dois tipos de _mount_, _gasket_ e _top-mount_.
  * Ter um _weight_ externo com opção de dois materiais, alumínio e aço inoxidável.
  * Não ter _flex-cuts_ na PCB e nas _plates_.
  * Uso de alumínio 6063.
  * Muitas ofertas ao nível de PCBs e plates.
  * Suporte de QMK/VIA.
  * O preço parece ser interessante para o que é oferecido.

### O que não aprecio

  * O criador afirma que o layout é _Alice_ em vários locais, mas na realidade não é. Será mais um misto de _Alice_ com _Arisu_. É o layout do _Spring_ da _Owlabs_.
  * Provavelmente a versão Bluetooth usará _software_ proprietário.
  * De momento não existe um vendedor para território europeu definido, o que significa que terá custos de importação.

### O que gostava de ver alterado

  * Uma USB _daughterboard_ que não é _open-source_.

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.