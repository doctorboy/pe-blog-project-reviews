# Krush65

![](assets/images/20230802-project-krush-65.jpeg)

**Data do último update:** 2/08/2023
**Maker:** @Nuxroskb (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?board=132.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0     |
| Specs                | 0.5     |
| Protótipos           | 0     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **4.5** |

### O que aprecio

  * Um _design_ exterior que pessoalmente acho bastante bonito. É basicamente um [Leviatan](https://perifericosdeescrita.substack.com/i/133001691/leviatan) com _layout_ de 65% na parte estética.
  * Um _weight_ externo em cobre de grandes dimensões.
  * Ter dois tipos de _mount_ bastante populares: _top_ _mount_ e o-_ring_ _mount_.
  * Suporte do _layout_ ISO.
  * O preço é muito interessante (da versão com _weight_ em cobre).
  * Utilização de materiais _premium_ como alumínio 6063 e cobre no _weight_.

### O que não aprecio

  * Ser uma cópia demasiado “descarada” do Leviatan.
    * Pessoalmente fico sempre um pouco de pé atrás quando isto acontece. Ainda para mais apenas poucos dias depois do GB do Leviatan ter terminado.
  * MOQ completamente desajustado à realidade do mercado internacional actual.

### O que gostava de ver alterado

  * Que tivesse um vendedor europeu.
  * Que o _weight_ estivesse exposto na parte interna do teclado, tal como é feito no Leviatan.
  * Gostava de ver mais informação sobre a parte eletrónica: PCB e uso ou não de USB _daughterboard_.

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.