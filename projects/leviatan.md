# Leviatán

![](assets/images/20230704-project-leviatan.jpeg)

**Data do último update:** 04/07/2023
**Maker:** Laminar Designs

## Interest Check

IC realizado antes desta iniciativa.    

## Group Buy

Link: [Geekhack GB](https://geekhack.org/index.php?topic=120681.0), [EloquentClicks Vendor](https://www.eloquentclicks.com/product/leviatan-mechanical-keyboard/), [Geekhack IC](https://geekhack.org/index.php?topic=118692.0)

### PROS
  * Um projecto que no geral demonstra muita atenção aos pequenos detalhes e de um criador que já tem experiência no _hobby_.
  * Um _weight_ externo e interno em cobre.
  * Três opções de _mounting_, _leaf-spring gasket_, _top mount_ e _plateless_ (o mesmo que o _leaf-spring, _mas sem a _plate_).
  * Três _layouts_ disponíveis, WKL, WK e HHKB (não é o verdadeiro HHKB).
  * _Typing angle_ de 9º, não é muito comum e é um dos meus favoritos.
  * Bons _bumpons_ de um _maker_ conhecido (pessoalmente preferia, no entanto, os _bumpons_ do Geonworks).
  * PCB com suporte do _layout_ ISO.
  * O peso de 2,4 — 2,6Kg para a versão completamente montada, indica que é um teclado bastante compacto.
  * Disponível no melhor vendedor europeu.
  * O preço entre 420€-470€ sem extras parece-me ser competitivo, para um dos melhores 60% deste ano.

### CONS
  * Quer o _top-mount_ como o _leaf-spring_ _gasket_ _mount_ têm as suas PCBs e plates. Se o utilizador tiver interessado em usar estes dois tipos de _mount_, terá que comprar as duas PCBs e as duas _plates_.
  * Utilização de uma USB _daughterboard_ que não é _open_-_source_. É, no entanto, uma versão de um criador conhecido e é expectável haver disponibilidade futura.
  * As _plates_ do projecto não suportarem o _layout_ ISO. No entanto, destacar o trabalho da EloquentClicks que irá disponibilizar _plates_ com suporte ISO especificamente para este projecto.

### Recomendo comprar?[^1]

- [x] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** O Leviatán é um dos projectos mais aguardados do ano para todos os amantes de 60%. Um projecto muito requintado, um teclado com linhas muito bonitas, que utiliza materiais de luxo e que estão na moda no hobby, como, por exemplo, o _weight_ em cobre. Permite também diversos tipos de configuração, quer ao nível de _mounting_ como ao nível de _layouts_. O criador é alguém com um passado no _hobby_ e de quem tenho visto bom _feedback_ consistentemente. A utilização de um vendedor europeu, é a cereja no topo do bolo, que, por exemplo, faltou noutro projecto recente de grande nível, o [Orbit65](https://perifericosdeescrita.substack.com/i/125061045/orbit). Tenho pena, que no meu ponto de vista, tenha apenas falhado na questão da USB _daughterboard_, penso que poderia utilizar a Unified S1, mas isso não invalida que seja um teclado muito promissor e que apresente na mesma um excelente valor para o que oferece. Boas compras!

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.