# Lightweight65

![](assets/images/20230715-project-lightweight-65.jpeg)

**Data do último update:** 17/07/2023
**Maker:** Holyswitch (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120789.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 0     |
| Protótipos           | 1     |
| Mounting visível     | 0     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **5** |

### O que aprecio
  * _Design_ simples e acessível.
    * Nota: o design dos pés em acrílico é me familiar. :)
  * Dois tipos de _case_, uma em acrílico e outra em FR4.
  * O preço alvo ($150) é competitivo para o que é oferecido, pois o MOQ deve ser relativamente baixo.
  * Suporte de QMK/VIA.

### O que não aprecio
  * Um IC que apenas tem como objectivo apresentar o projecto à comunidade, visto que o produto vai estar à venda dentro de 1-2 semanas.
  * Apenas disponibilizar um _layout_ fixo.

### O que gostava de ver alterado
  * Esteticamente, penso que falta um bocadinho de altura ao acrílico na parte superior para que os _switches_ não fiquem demasiado expostos.
  * Gostava de ver mais informação sobre a _plate_.
  * Devido aos poucos layouts suportados na versão _hotswap_, seria interessante ver disponível uma _solderable_ PCB.
  * Não suporte do _layout_ ISO.
  * A case em FR4 não apresenta nenhum tipo de ângulo. Gostava que uma solução fosse apresentada para este tipo de _case_.

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=121010.0)

### PROS

  * Um _design_ simples para os apreciadores de teclados em acrílico.
  * O preço para o que é oferecido ($100, $50 mais baixo do que o valor falado no IC) é bom.
  * Suporte de QMK e VIA.

### CONS

  * Apenas um _layout_ disponível. Não suporta ISO.
  * _Layouts_ suportados pela solderable PCB que será opcional, são desconhecidos.
  * _Design_ da _plate_ é desconhecido.
  * Não tem vendedor na Europa, logo irá implicar custos de importação.
  * A proposta de valor de um teclado deste género não é tão forte como a de um teclado com materiais mais _premium_, como, por exemplo, em alumínio.
  * O site do criador que vai realizar a venda não inspira muita confiança.

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [x] Não recomendo

**Comentário:** O Lightweight65 é um projecto simples de um criador que se está a iniciar no hobby e decidiu criar o seu primeiro teclado. O Lightweight65 tem duas opções de materiais para a _case_, acrílico e FR4 e usa um mount bastante característico neste tipo de teclados, o _sandwich_ _mount_. Este tipo de projectos eram bastante populares à coisa de 3-4 anos atrás no hobby, mas hoje em dia são bastante difíceis de se tornar boas propostas de valor, tendo em conta a concorrência que enfrentam e que usam materiais mais _premium_. Como tal, tenho algumas dificuldades em recomendar este tipo de teclados que sejam bastante simples quando temos propostas mais interessantes na globalidade por preços finais muito semelhantes (ver, por exemplo, o que falei em cima para o Neo65). Se gosta de teclados em acrílico, e está a iniciar-se no _hobby_, o Lightweight65 pode fazer todo o sentido. Se, por outro lado, gosta de teclados em acrílico, mas gostava de experimentar algo mais diferenciado, aconselho-o a continuar a ler, pois falarei a seguir de outro projecto que poderá fazer mais sentido nessa área.

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.