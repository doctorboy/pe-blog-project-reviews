# Luminkey80

![](assets/images/20230802-project-luminkey-80-tkl.jpeg)

**Data do último update:** 02/08/2023
**Maker:** Createkeebs

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120944.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 0     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 1     |
| **Total**            | **5** |

### O que aprecio

  * _Design_ simples para um TKL.
  * Utilização de materiais _premium_, como o cobre no _weight_ e alumínio 6063 na _case_.
  * Utilizar uma peça para separar a bateria do resto do teclado.
  * Utilização de uma PCB _hotswap_ _multi-layout_ com suporte de ISO.
  * A _exploded view_ está bem explicada. Ponto extra na tabela.

### O que não aprecio

  * Não disponibilizar IC _form_ para recolher _feedback_ de modo estruturado.
  * Ter disponível apenas uma versão _wireless_.
  * Usar o conceito de espuma por todo o lado por defeito.
  * O _weight_ ser apenas externo e de pequenas dimensões.

  *Para o que é oferecido e tendo em contas as opções disponíveis, o preço não me parece o mais competitivo.

### O que gostava de ver alterado

  * Que fosse disponibilizado uma opção de PCB sem ser apenas _hotswap_.
    * Nos comentários dá a ideia de que haverá uma _solderable_ PCB.
  * Tenho dúvidas sobre o que significa o kit _assembled_: será que inclui _keycaps_?

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.