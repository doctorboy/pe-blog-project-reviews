# LZ CPs

![](assets/images/20230627-project-lz-cp.jpeg)

**Data do último update:** 27/06/2023
**Maker:** LZ

## Interest Check

IC realizado antes desta iniciativa.   

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120603.0)

### PROS

  * O LZ-CPs é a nova versão do LZ-CP, um 1800s com duas cores.
  * O _weight_ é em aço carbono e tem um aspecto visual muito agradável (opinião pessoal).

### CONS

  * A modernização do projecto foi mínima.
  * A qualidade apresentada nos protótipos está bastante longe do que é apresentado noutros projectos.
    * Vejam, por exemplo, como é fácil ver as manchas na anodização ao pé dos buracos para os parafusos.
    * Importa referir que também existem outras imagens de protótipos iniciais que apresentam uma qualidade muito superior ao nível dos acabamentos, mas são estas de má qualidade que estão a ser publicitadas para a venda.
  * Ausência de fotos das partes interiores do teclado.
    * Fica a dúvida qual a USB-C _daughterboard_ que é utilizada.
  * A presença de um nota na venda que indica que algumas falhas nos acabamentos poderão existir e não são razão para reportar um produto com defeito.

	- ![](assets/images/20230627-project-lz-cp-2.jpeg)
- A ausência de vendedor na Europa.
* O preço base desajustado de $790. Com importação é teclado para passar os 1000€ sem ter um único aspecto de destaque.
* O criador, LZ, é um criador bastante conhecido no hobby e com registo de já ter lançado muitos teclados. No entanto, as suas últimas criações têm apresentado baixos níveis de qualidade.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [x] Não recomendo

**Comentário:** O LZ CPs é a reedição do LZ-CP, um teclado que foi originalmente apresentado em 2015. Infelizmente, os projetos mais recentes do criador LZ têm sido bastante decepcionantes, e é com desilusão e tristeza que vemos alguns dos clássicos do passado não receberem a modernização que seria esperada. As fotos apresentadas do protótipo sugerem que este projecto está novamente muito longe dos padrões de qualidade e inovação dos dias actuais. Se juntarmos a isso, o facto do preço base ser muito elevado (quase $800), e não termos um distribuidor na Europa, torna este projecto não recomendável de todo. Até que haja evidências em contrário, eu não consigo aconselhar ninguém a avançar com a compra de projectos relacionados com o LZ, infelizmente.

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.