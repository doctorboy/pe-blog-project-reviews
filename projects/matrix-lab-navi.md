# Matrix Lab Navi
A proposta FRL TKL da Matrix Labs.

![](assets/images/20230606-project-matrix-lab-navi.jpeg)

**Data do último update:** 06/06/2023
**Maker:** Matrix Lab

## Interest Check

IC realizado antes desta iniciativa.  

## Group Buy

Link: [Página Oficial](https://matrixlab.notion.site/MATRIX-LAB-NAVI-3549cc8843ed4d9db02b5bf1d65e2521)

### PROS

- Em termos de design é um Matrix Lab: muitos _badges_, bonecos, várias peças acessórias, etc. Para quem gosta, não vai ficar desiludido.
    - Para os fãs deste tipo de _design_, certamente que vão gostar do pequeno ecrã LCD que pode mostrar GIFs com resolução 128x128.
- As cores, os acabamentos, etc., Matrix Labs não desilude.
- Vendedor na Europa através da Oblotzky Industries.
- A _plate_ ser separada em duas partes e haver uma versão que não tem _flex cuts_.

### CONS

- Apenas disponível com PCB _hotswap_, é um pouco incompreensível num produto de gama alta numa marca de renome no hobby.
- Não suporta ISO (num produto de gama alta).
- A USB _daughterboard_ não parece ser um modelo _open-source_.
- Os _weights_ serem apenas externos.
- Um 70% no formato, mas que na realidade é um 65%. Este foi o compromisso que foi encontrado de modo a manterem a sua identidade na parte estética. Pessoalmente não aprecio.
- No momento que escrevo esta _review_, o preço na EU ainda não foi publicitado, mas dificilmente será um preço atrativo. Se o preço final for entre 500-550€, é caro mas tolerável e na linha de produtos recentes da marca, mas se for mais elevado, acho que é bastante desajustado.

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [x] Não recomendo

**Comentário:** O Navi é a proposta da Matrix Labs para o novo layout do momento, os 70%. Esquecendo o facto de que para manter a sua identidade, a Matrix Labs transformou o seu 70% num 65%, a falha de apenas disponibilizar uma PCB hotswap, não suportar ISO, e ser um produto caro, o Navi é um produto que irá certamente agradar os fãs deste tipo de design. No entanto, considerando o que a concorrência está a tentar trazer para o mercado (ver projecto [Violetta 70](violetta-70.md) ) e as falhas apontadas, esta é uma compra que eu não posso recomendar. A escolha da PCB em ser apenas _hotswap_, é uma opção confusa tendo em conta que a audiência de um produto deste género deverá ser os utilizadores mais experientes e conhecedores no hobby. Pessoalmente também me custa bastante perceber como a Matrix Labs pretende entrar mais no mercado europeu, e não oferece suporte ao layout ISO na sua PCB (ainda para mais quando em modelos anteriores já houve suporte a ISO). De referir que o Navi traz um _layout_ muito particular e arranjar alternativas à PCB original poderá ser bastante difícil.  
Como nota final, o Matrix Labs Navi, é um produto interessante e com qualidade, mas na minha opinião falha redondamente nas opções disponibilizadas, prejudicando a sua proposta de valor.

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.