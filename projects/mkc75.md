# MKC75

![](assets/images/20230818-project-mkc75.jpeg)

**Data do último update:** 18/08/2023
**Maker:** MyKeyClub

## Interest Check

IC realizado antes desta iniciativa.   

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=121033.0), [Página de Produto](https://www.mykeyclub.com/pages/mkc75-preorder-page)

### PROS

  * Um 75% visualmente interessante e com um preço super competitivo.
  * Utilização de alumínio 6063.
  * Três tipos de _mounting_: _top_ _mount_ e _plate_/PCB _gasket_ _mount_.
    * Suporta _plateless_.
  * Utilização de um _weight_ interno (alumínio) e outro externo (alumínio ou aço inoxidável).
    * Ter a opção de ter um _weight_ em aço inoxidável é muito interessante para um teclado de gama de entrada como este.
  * Muita variedade nos acabamentos do _weight_.
  * _Plates_ e PCB disponíveis sem _flex_ _cuts_.
  * Suporte de _layout_ ISO tanto na _solderable_ PCB como na opção _hotswap_.
  * Suporte de QMK/VIA.
  * Boa oferta ao nível de cores.
  * Disponível em mais que um vendedor europeu, o que é fantástico.
  * O preço é super competitivo (aproximadamente 150€, uma configuração já com um _weight_ em aço inoxidável e acabamento PVD).

### CONS

  * O _mounting_ _top_ _mount_ apresenta um posicionamento dos pontos de fixação que deixa muito a desejar.
  * A versão sem fios, que usa _bluetooth_, não usa _firmware_ _open-source_.
  * A apresentação do projecto não mostra fotos dos protótipos.
  * Necessidade de se utilizar pequenos pedaços de espuma no _mount_ da _case_ para melhorar a parte sonora.
  * Ao nível sonoro, com a espuma no seu interior, tem um perfil sonoro típico de teclados com espuma. Fica a dúvida como se comporta sem esta (a minha expectativa é que irá comportar-se bem).
  * Um pormenor: o _cutout_ no _weight_ interno para se aceder ao cabo da USB _daughterboard_ não me parece muito prático.

### Recomendo comprar?[^1]

- [x] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** O MKC75 é uma proposta muito interessante da Mykeyclub para todos os que estão a entrar no hobby e querem um teclado um pouco mais compacto que um TKL. Foi com um 75% que eu também entrei neste hobby há mais de cinco anos e o MKC75 apresenta uma das melhores propostas de valor de momento. Está longe de ser um teclado perfeito, como se pode ver em cima em alguns pontos que realcei, mas por pouco mais de 150€ é difícil pedir mais do que é oferecido. Se está a entrar no hobby ou simplesmente procura um teclado de preço mais acessível, com layout 75%, e se não aproveitou outras boas opções como o Jris75 ou QK75, esta é a sua oportunidade para ter um teclado de boa qualidade sem gastar uma fortuna. O MKC75 encontra-se já disponível em GB em diversos vendedores europeus até ao final do mês de Agosto.

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.