# Molly60

![](assets/images/20230605-project-molly-60.jpeg)

**Data do último update:** 5/06/2023
**Maker:** Aregs

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120432.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0     |
| Specs                | 0.5     |
| Protótipos           | 0     |
| Mounting visível     | 0     |
| Layouts suportados   | 0     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **2.5** |

### O que aprecio

  * Muitas cores disponíveis.
  * O preço é muito competitivo.
  * Parece suportar ISO, olhando para a página de produto (mas como é tudo um pouco confuso, não tenho certeza).

### O que não aprecio

  * O peso de 2Kg para um 60% HHKB seria óptimo, no entanto, tenho muitas dúvidas que este valor esteja correcto quando o _weight_ parece ser pequeno.
  * A qualidade de controlo descrita para este produto, não representa padrões de qualidade elevados. No entanto, por $99 é difícil pedir mais.
  * Um IC que é apenas uma jogada de marketing pré-venda para o GB que ainda vai decorrer este mês.
    * Não existe grande propósito de recolher _feedback_ para melhorar o teclado, apenas perceber qual o volume de vendas expectável.
  * Não existirem fotos de unidades reais. O projeto no geral parece ser um bocado suspeito, considerando quem é o criador, informação disponibilizada, etc.

### O que gostava de ver alterado

  * Informação sobre que tipo de alumínio está a ser utilizado.
  * Informação e imagens/renderizações sobre as partes internas do teclado.
  

## Group Buy

Link: [Página de venda internacional](https://nlandkeys.com/products/molly60-keyboard)

### PROS

  * Muitas cores disponíveis.
  * Dois _layouts_ suportados, WK e HHKB (não é o verdadeiro).
  * Preço acessível (começa nos $99).
  * Suporte de QMK e VIA.
  * Um _weight_ externo.
  * Suporte de ISO com PCB _hotswap._

### CONS

  * O projecto na sua globalidade é um pouco suspeito.
    * Não existe informação partilhada sobre o interior do teclado.
    * Não é possível observar quais os _mounting_ _points_, como é feito o _mount_, como é o _weight_, etc.
      * No [vídeo do Alexotos](https://www.youtube.com/watch?v=j5wzxyY6G0Y), consegui ver alguma desta informação.
    * Não existem fotos de protótipos.
    * Se quiseres saber mais e tiveres interessado, aconselho a veres o [stream do Alexotos](https://www.youtube.com/watch?v=j5wzxyY6G0Y).
  * A plate em policarbonato está cheia de _flex_ _cuts_.
  * O acabamento da case é _spray_ _coated_, que não tem a mesma qualidade que outros acabamentos comuns no _hobby_ como a anodização.
  * O _weight_ é de pequenas dimensões.
  * Sem vendedor na Europa, o que implica custos de importação.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [x] Não recomendo

**Comentário:** O Molly é um 60% com alguns compromissos na qualidade de modo a atingir um preço bastante baixo. Infelizmente, como não existe vendedor europeu, o preço acaba por ser penalizado e destruir o argumento mais forte deste produto no mercado europeu. Pessoalmente, a menos que a estética do teclado diga-me muito, preferia investir um pouco mais e ter um produto com uma qualidade muito superior e disponível directamente no mercado europeu, como, por exemplo, o [QK60](qk-60-r2.md) que apresenta melhores acabamentos em toda a linha, mais opções e tem uma boa reputação de entregar produtos com qualidade.

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.