# Muramasa
The ultimate longboi

![](assets/images/20230605-project-muramasa-xt-frl-numpad.jpeg)

**Data do último update:** 05/06/2023
**Maker:** @masje (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120482.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 0     |
| Mounting visível     | 0.5     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **5.5** |

### O que aprecio

  * Ter _weights_ internos de dimensões significativas em latão.
    * O criador está a considerar também aço inoxidável.
  * O projecto vai ser aberto para a comunidade após os primeiros protótipos.
  * Para 100 MOQ o preço alvo de $480 parece ser bastante competitivo.

### O que não aprecio

  * O peso de 3333g de certeza não é verdadeiro e apenas um _meme_.
    * Sem protótipos, qualquer valor não passa de uma estimativa (principalmente quando nem o _design_ do teclado está fechado).

### O que gostava de ver alterado

  * O projecto encontra-se numa fase muito inicial, onde o _mounting_ não está definido e as renderizações apresentam diferentes _designs_ para o perfil lateral do teclado. Falta muita informação.
  * Uma das renderizações tem uma falha visual que leva a pensar que as teclas direcionais têm uma _plate_ própria. Nas restantes imagens e no texto dá para perceber que não é o caso.


## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.