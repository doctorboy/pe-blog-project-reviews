# Navi60

![](assets/images/20230802-project-navi-60.jpeg)

**Data do último update:** 02/08/2023
**Maker:** Psychetype (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120984.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 0     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **6** |

### O que aprecio

  * Dois tipos de _mount_ bastante clássicos: _top_ _mount_ e o-_ring_ _mount_.
  * Um _weight_ externo.
  * Utilização de materiais _premium_ como um _weight_ em latão e uma _case_ em alumínio 6063.
  * Utilização de uma USB _daughterboard_ _open_ _source_.
  * Suporte de QMK/VIA
  * Suporte de ISO (quer na versão _solderable_ da PCB como numa PCB _hotswap_ dedicada).
  * Ao nível de _design_ gosto do facto de não ser um rectângulo puro e ter algumas curvas.
  * Disponível via vendedor europeu.

### O que não aprecio

  * As fotos do protótipo não são as melhores.

### O que gostava de ver alterado

  * Imagens sobre as partes internas do teclado. Difícil de avaliar o teclado como um todo sem avaliar as partes internas.
  * Gostava que tivesse mais opções ao nível de materiais para a plate, mas não é critico desde que o design seja partilhado à posteriori.
  * O preço é bom ($200), mas existem melhores ofertas ao nível de valor no mercado.

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.