# Neson Design 810E

![](assets/images/20230613-project-neson-design-810e.jpeg)

**Data do último update:** 13/06/2023
**Maker:** Neson Design

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120539.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 0.5     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **6.5** |

### O que aprecio

  * Parece ter um ou dois _weights_ externos.
  * Para os amantes de _layouts_ onde a barra de espaços fica isolada, está aqui um projecto FRL 1800 para aproveitarem.
  * Suporte de ISO e de uma espécie de HHKB FRL 1800.
  * Opção de PCB sem _flex cuts_.

### O que não aprecio

  * Não é muito claro como é feito o _mount_, mas pelo tamanho dos _gaskets_, não me parece que seja _leafspring_ como o criador indica.
  * Não são referenciados distribuidores ou revendedores, como tal para terras lusas deve requerer importação. No IC _form_ são questionados quais os revendedores pretendidos, como tal este ponto ainda poderá mudar.
  * Terem agregado a PCB com os leds à USB _daughterboard_. Deste modo para além de usarem uma USB _daughterboard_ proprietária, o custo desta ainda deverá ser significativo caso seja preciso substituir.

### O que gostava de ver alterado

  * O teclado acaba por ter uma construção mais complicada do que originalmente aparenta, e ao ser composto por tantas partes, não facilita este apresentar um preço competitivo.
  * Suportar barra de espaços 7u na _solderable_ PCB.
  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.