# Orbit 65

![](assets/images/20230531-project-orbit-65-machina.jpeg)

**Data do último update:** 31/05/2023
**Maker:** Machina

## Interest Check

IC realizado antes desta iniciativa.  

## Group Buy

Link: [Página oficial](https://machina.mx/products/orbit?variant=44865584038162), [IC no Geekhack](https://geekhack.org/index.php?topic=119018.0), [Fotos do Markerchun](https://markerchun.com/orbit)

### PROS

- Pessoalmente adoro o design. A curva do profile lateral é muito particular e não deve ser propriamente barato de fabricar.
- A opção de ter dois tipos de partes inferiores, uma em alumínio com _weights_ internos em latão e outra onde a parte inferior do teclado tem um design interno próprio e é 100% em latão ou cobre.
- O pormenor de tapar a USB _daughterboard_ com uma peça em policarbonato.
- A plate não ter _cutouts_ que muitas vezes acrescentam pouco.
- Não precisa de espuma, nem é vendido com espuma.
- O _mounting_ usa tadpoles, o que parece ser uma boa opção para um teclado deste género.

### CONS

- A PCB _hotswap_ não tem suporte ISO.
- Não oferece diversos tipos de dureza para os _tadpoles_. Uma oportunidade perdida para os utilizadores poderem ajustar o _typing feel_.
- Um pormenor: outra oportunidade perdida de esconder o _cutout_ para o cabo da USB _daughterboard_ debaixo da peça que cobre a USB _daughterboard_.

### Recomendo comprar?[^1]

- [x] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** O Orbit é provavelmente, em termos teóricos, um dos melhores 65% dos últimos tempos. Em termos de design, é bastante diferenciado, tem _weights_ grandes, em materiais e acabamentos nobres. O criador demonstra muita atenção ao detalhe e o preço é super competitivo para o que o teclado oferece. Infelizmente, para o mercado europeu, não existe um distribuidor, o que acaba por tornar o preço deste teclado um bocado caro, visto que irá requerer custos de importação. Mesmo assim arrisco a dizer que é um bom negócio. O Orbit 65 faz-me lembrar bastante outro teclado, o Monokei Hiro, onde o preço era bastante mais elevado. Pessoalmente acredito que o Orbit tem potencial para ser melhor que o Hiro e tornar-se numa das referências nos 65%.

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.