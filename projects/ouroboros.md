# Ouroboros

![](assets/images/20230717-project-ouroboros.jpeg)

**Data do último update:** 17/07/2023
**Maker:** Bachoo

## Interest Check
  
IC realizado antes desta iniciativa.

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120811.0)

### PROS

  * Um 60% com um _mount_ _plateless_ _gasket_ de um criador reputado no hobby.
  * Dois _layouts_ disponíveis, WKL e WK.
  * _Weight_ externo em aço inoxidável.
    * Com um _design_ que pessoalmente acho bonito.
  * Suporte de QMK, VIA e Vial.
  * Uso de USB _daughterboard_ _open_ _source_.
  * Suporte de _layout_ ISO.
  * PCB _open_ _source_, mesmo antes do GB.

### CONS

  * O _weight_ é de pequenas dimensões e apenas externo.
  * O peso é baixo para um 60%, aproximadamente 1 Kg antes de montar.
  * Sem vendedor europeu, o que significa que teremos de considerar custos de importação em território europeu.
  * O preço base de $400, considerando um MOQ de 100 unidades, acho também pessoalmente elevado, considerando outros projectos como o [Leviatán](leviatan.md), ou mesmo projectos com MOQ mais baixos como o [Unagi](unagi-wkl-60.md).


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [x] Não recomendo

**Comentário:** O Ouroboros é um projecto interessante para os amantes de 60%. Tem um _design_ simples, é de um _maker_ reputado no _hobby_, e foi desenvolvido à volta de um _mount_ _gasket_ _plateless_ que na teoria irá proporcionar um bom _typing_ _feel_. No entanto, não podemos deixar de considerar que o preço base em mercado europeu são $400 + portes + custo de importação, o que faz deste projecto uma escolha difícil de optar, quando temos outros projectos muito bons a decorrer neste momento. Para comparação, o  [Leviatán](leviatan.md) (a começar nos 420€ já na Europa) apresenta neste momento uma proposta de valor superior (muitas características semelhantes, mas com um _weight_ bem grande em cobre que faz o teclado pesar mais do dobro do que o Ouroboros). De qualquer forma, se o preço não for problema, o Ouroboros apresenta vários indicadores de ser um bom teclado que não irá desiludir os seus utilizadores. Gostava de referir, por último, que o criador do projecto, Bachoo, tornou público todos os ficheiros relacionados com a criação da PCB. Uma opção de louvar e que permite que qualquer pessoa possa mandar fazer (ou melhorar/actualizar) PCBs para o Ouroboros se assim o precisar.

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.