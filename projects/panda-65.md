# Panda 65

![](assets/images/20230626-project-panda-65.jpeg)

**Data do último update:** 26/06/2023
**Maker:** @goju2506 (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120646.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0     |
| Specs                | 0.5     |
| Protótipos           | 1     |
| Mounting visível     | 0     |
| Layouts suportados   | 0     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **3.5** |

### O que aprecio

  * Em termos de _design_, é algo sólido e com provas dadas noutros teclados.
  * Utilização de alumínio 6063.
  * O MOQ do projecto (50 unidades) parece-me ajustado.
  * É mencionado a utilização de dois _weights_ em aço inoxidável, com acabamentos diferentes. É provável que estes _weights_ sejam apenas exteriores.

### O que não aprecio

  * Não existem imagens do interior, por isso torna-se difícil fazer uma análise mais detalhada do teclado em si.
  * Não existe informação sobre os *layouts* suportados, etc.
  * O IC _form_ apenas questiona se está interessado em comprar e qual a cor. Não existe um foco em recolher feedback, como tal devemos assumir que este projecto apresenta já o seu _design_ final.
  * O preço, sem mais informação, parece-me um pouco elevado.

### O que gostava de ver alterado

  * Ao nível de _design_ parece ser demasiado inspirado em teclados como o Unikorn da Singa, onde praticamente apenas muda o _weight_. Aceitável, mas preferia ver mais toques próprios (como, por exemplo, o chanfro em preto).


## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.