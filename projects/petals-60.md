# Petals60

![](assets/images/20230524-project-petals-60.jpeg)

**Data do último update:** 24/05/2023
**Maker:** Sourmk

## Interest Check

IC realizado antes desta iniciativa.  

## Group Buy

Link: [Website no vendor Sandkeys](https://sandkeys.me/product/petals-60/), [Geekhack IC](https://geekhack.org/index.php?topic=120064.0)

### PROS

  * Usa materiais premium como alumínio 6063 e _brass_ para o weight.
  * O _weight_ é interno e externo, de modo a maximizar o seu benefício.
  * O preço parece-me ligeiramente elevado para o que oferece, mas houve um esforço claro do criador em tentar oferecer um _proxy_ europeu ([Qoda](https://qoda.store/products/petals-sixty-case?variant=46595824517458)). Isso é de valor!
  * Numero de cores disponibilizadas é interessante para um projecto desta dimensão.
  * Os _pads_ do teclado, são os mesmo utilizados em teclados como o Geonworks F1 e são de qualidade bem acima da média. Este é apenas um exemplo de um pormenor, que demonstra atenção aos detalhes por parte do criador.
  * Gostava de realçar o conceito de o Petals ter sido desenhado para utilizar PCBs universais 60%.
    * No entanto, pessoalmente, se o argumento é não prender os utilizadores com algum _design_ proprietário de PCB, era preferível fazerem o seu _design_ e depois simplesmente liberarem o projecto da PCB como _open-source_ (já houve casos idênticos no passado). Deste modo, o _design_ escolhido tem limitações como as faladas na secção seguinte.
    * Para além disso, uma PCB universal 60% dos dias de hoje, não será igual a uma PCB universal de amanhã (a tecnologia evolui). Um exemplo disso é a porta do lado esquerdo, que está a cair em desuso. Mais uma vez, uma amostra de que o _design_ no geral é um pouco antiquado na minha opinião.
  * Teclado de um criador com reputação sólida no _hobby_.

### CONS

  * Este ponto é subjectivo, mas acho o _design_ do Petals demasiado antiquado. Tem um pequeno _lip_ no _bezel_ superior e inferior, bem como algumas curvas, mas o restante _design_ é antiquado, semelhantes a teclados de à 2-3 anos. De realçar, novamente, que isto é apenas uma opinião pessoal, e bastante subjectiva.
  * 60€ por PCBs extra _hotswap_ de um teclado 60% é exagerado no meu ponto de vista (mesmo esta sendo da autoria do Wilba).
  * Utilização por defeito de uma plate em policarbonato (que é uma óptima escolha por defeito) origina algum flex, que poderá ser problemático para a porta USB, visto que este teclado não usa uma USB _daughterboard_. Ou seja, o _flex_ presente no teclado estará presente também na porta/cabo USB e poderá danificar esta.
  * Não tem muitas ofertas de base ao nível de materiais para a plate.



### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** Se é fã de 60%, e gosta do _design_ do Petals, dificilmente ficará desiludido com este teclado. No entanto, tendo em conta o preço e a oferta actual, pessoalmente acho que existem projectos mais interessantes que o Petals na área dos 60%. O Petals acaba por se situar numa zona onde se enquadra como um projecto interessante e onde o criador demonstra bastante atenção com pequenos pormenores, mas que depois acaba por não ter um preço muito atrativo e também por não trazer nada de muito inovador.


[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.