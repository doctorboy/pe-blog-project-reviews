# Pianoforte TKL

![](assets/images/20230822-project-pianoforte-f13-tkl.jpeg)

**Data do último update:** 22/08/2023
**Maker:** Hayasaka (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=121128.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **5** |

### O que aprecio

  * Dois tipos de _mount_, _gasket_ _mount_ e _o-ring mount_ (este último ainda pode mudar para um _top_ _mount_)
  * Utilização de um _weight_ externo.
  * Utilização de materiais _premium_ (alumínio 6063 e aço inoxidável quer no _weight_ como na peça do meio da estrutura do teclado).
  * Suporte de ISO com a _solderable_ PCB.
  * _Plate_ sem _meme_ _cuts_.
  * Utilização de uma USB _daughterboard_ _open_-_source_.

### O que não aprecio

  * Não apresenta um IC _form_ para processar o _feedback_ de um modo mais estruturado.
  * Apresenta imagens de _exploded_ _views_ de uma versão do teclado que não irá ser vendida. Pode levar a engano os mais desatentos.
  * Considerando um MOQ de 300-500 unidades, o preço de $500-$600 é demasiado elevado para o que o teclado oferece.

### O que gostava de ver alterado

  * Os protótipos apresentam alguns problemas de qualidade. Apesar de ser normal e aceitável para um protótipo, não é um grande indicador para a qualidade final do produto.

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.