# Pixelspace Shadow 80% TKL

![](assets/images/20230509-project-pixelspace-shadow-80-tkl.jpeg)

**Data do último update:** 09/05/2023
**Maker:** Pixelspace Studio / ClickClack

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120254.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0.5   |
| Specs                | 1     |
| Protótipos           | 0     |
| Mounting visível     | 0     |
| Layouts suportados   | 1     |
| Preço Estimado       | 0     |
| Bónus                | 0     |
| **Total**            | **3.5** | 

### O que aprecio

- O parte intermédia do teclado ser em stainless steel na versão "stainless steel".

### O que não aprecio

- O não uso de uma daughterboard que seja open-source para a porta USB-C.
-   Ter apenas weights externos e nenhum interno (mas não é algo crítico, apenas gosto pessoal).
-   Ter como uma das duas opções uma versão toda em alumínio. Não me parece que faça muito sentido ao ter múltiplas partes. Preferia que esta versão fosse substituída por uma versão mista entre alumínio e stainless steel (ou outro material).
-   Um projecto desenhado e criado para um número elevado de vendas, mas que ainda não tem protótipos realizados (ou pelo menos não existem fotos deste) quando é apresentado à comunidade.
-   Ter planeado espuma quando ainda não existe um protótipo.

### O que gostava de ver alterado

-  O mounting menciona ser gasket leaf-spring, mas as fotos partilhadas não mostram claramente o mouting. Gostava de ver mais informação.
- Pessoalmente preferia que a daughterboard USB-C fosse desacoplada da tira de leds, para ser mais fácil e barato substituir.
- Falta informação sobre o peso do teclado, mas presumo que esta informação não esteja disponível porque não existem protótipos ainda.
- Mais informação sobre as PCBs, Plates, etc.


## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.