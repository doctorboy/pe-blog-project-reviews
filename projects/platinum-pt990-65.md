# Platinum PT990

![](assets/images/20230524-project-platinum-pt990-65.jpeg)

**Data do último update:** 24/05/2023
**Maker:** Percent Studio

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120285.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0.5     |
| Specs                | 0.5     |
| Protótipos           | 0     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 0     |
| Bónus                | 0     |
| **Total**            | **4** |

### O que aprecio

  * Noção por parte do criador de conceitos como a continuidade de uma curvatura e os respectivos ajustes feitos no desenho deste teclado.
  * A utilização de um _mounting_ _leaf-spring_, originalmente lançado no teclado Owlab Spring.
    * É um _mounting_ interessante, mas na minha opinião difícil de trabalhar, quer ao nível de montagem como ao nível de optimização da sensação a ser transmitida pelo teclado aquando da sua utilização.
  * Tem cores interessantes e fora do comum.
  * O _weight_ parece ser de significativa dimensão e está disponível em materiais _premium_ como aço inoxidável ou cobre.

### O que não aprecio

  * Toda a conversa de _marketing_ à volta da apresentação do projecto. Aceitaria completamente se fosse um Group Buy, mas para um _Interest_ _Check_, acho despropositado.
    * Exemplos: "_the keyboard produces a very high-quality audio experience, comparable to that of a high-fidelity audio system_".
  * Vender a ideia que com espuma no seu interior o teclado é melhor do que um teclado sem espuma. O que é conceptualmente impreciso.
    * "_This design innovation significantly improves the keyboard's overall performance compared to those with conventional inner structures._"

### O que gostava de ver alterado

  * Apenas oferecer opção com _hotswap_ e com _layout_ fixo. Sem suporte para ISO, o que é bastante desapontante para um teclado que aspira vender bastante.
  * Podia e devia ter mais informação sobre as especificações do teclado (ex. altura, peso, etc.).
  * Gostava de ver um preço alvo para este projecto. Devido ao _design_ não espero que seja baixo.


  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.