# Project-EC

![](assets/images/20230717-project-project-ec-topre-60.jpeg)

**Data do último update:** Adicionar data
**Maker:** Synthetics e bowlkeyboards (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120851.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0.5     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **6.5** |

### O que aprecio

  * Um projecto à volta de Topre, é sempre positivo.
  * Suporte dos _layouts_ 6u HHKB (o verdadeiro), WKL e WK.
  * _Design_ com identidade própria, que expõe o _weight_ interno ao exterior em várias zonas.
  * Utilização de alumínio 6063.
  * Acho interessante o _mounting_, um silicone _gasket_ _mount_.
    * Destaco a atenção ao detalhe do criador, ao tentar colocar pequenas saliências no _gasket_ para que este fique “preso” na _case_.
  * Utilização de uma USB _daughterboard_ _open_ _source_ (neste caso o modelo mais recente).
  * _Weight_ interno e externo, em aço inoxidável.
  * Utilização de uma PCB _open_ _source_.

### O que não aprecio

  * O peso de 1.3 Kg parece-me um pouco baixo para o tamanho do _weight_. Provavelmente o _weight_ não ocupa aquele espaço na totalidade. A rever no futuro.

### O que gostava de ver alterado

  * O preço alvo de $450 parece-me ser demasiado elevado, se considerarmos que tem um MOQ de 100 unidades e o peso de 1.3 Kg.
  * Utilizarem _bumpons_ melhores que os que são mostrados nos protótipos.
  * O MOQ de 100 unidades parece-me ambicioso, preferia um valor mais baixo.

  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.