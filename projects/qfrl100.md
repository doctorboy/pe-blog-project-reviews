# QFRL100

![](assets/images/20230704-project-qfrl100.jpeg)

**Data do último update:** 04/07/2023
**Maker:** Thebloodyez

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120750.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0.5     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **6.5** |

### O que aprecio

- Layout 100% FRL, que não tem tido muitos projectos ao longo dos últimos anos.
- Três _weights_ internos
- Os diversos layouts que suporta, ISO, _split spacebar_, 10u _spacebar_, etc.
- Ter dois _mounts_ diferentes, o clássico _top mount_ e um _mount_ mais inovador, uma espécie de _leafspring_ _bottom mounting_.
- Usar uma USB _daughterboard open-source_.
- Suporte na PCB para _Alps_ _switches_
- O preço de $410 para um MOQ de 100 unidades, parece ser aceitável.
	- O fabricante escolhido é uma das melhores fábricas com conhecimento sobre teclados na China. Bom indicador da possível qualidade final do produto.


### O que não aprecio
- Apontar para um valor de MOQ relativamente alto, com um layout que não é muito popular, pode ser difícil de atingir.
- Ainda sem informação sobre a utilização de vendedores na Europa. Algo a verificar aquando do GB.


### O que gostava de ver alterado
- Localização da USB, acho que ficaria melhor se fosse centralizada.
- Gostava que a PCB já tivesse suporte para os novos conectores Molex da nova versão da USB _Unified Daughterboard_.

  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.