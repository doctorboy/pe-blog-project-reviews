# Cub65
Subtitulo

![](assets/images/20230524-project-qk-cub65.jpeg)

**Data do último update:** 24/05/2023
**Maker:** QwertyKeys e Cub Studio

## Interest Check

Link: [Reddit](https://www.reddit.com/r/mechmarket/comments/137cxnw/ic_cub65_qwertykeys_cub_studios_1st_collaborative/)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 0.5     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **5.5** |

### O que aprecio

- Gosto que haja já diversos protótipos do teclado em questão.
- Cores interessantes e variadas, como já é costume com os projectos da QwertyKeys.
	- Ter diferentes versões dos badges também me parece interessante para quem pretende mais customização.
- Suportar QMK/VIA/VIAL, o que é importante num teclado para massas.
- Ter PCBs sem flex cutouts (pelas imagens parece que os cutouts que existem são uniformes nos alphas, o que é de realçar).
- Acho interessante ter um angulo de 8º num 65%, tenho ideia que não é muito comum.

### O que não aprecio

- Não é a primeira nem a segunda vez que os projectos da QwertyKeys e Owlabs não usam versões open source da USB daughterboard, o que é desapontante.
- O teclado seguir o perfil sonoro da QwertyKeys que consiste em colocar 3 níveis de espuma no seu interior. Fica a dúvida como se comporta sem esta espuma.
- O conceito geral destes teclados que simplesmente se focam na premissa do design e repetem a formula consecutivamente sem grande inovação. Existe pouca preocupação com aspectos sonoros (simplesmente usam espuma no interior) e o mounting tende sempre a ser semelhante. Pelos preços praticados, costumam ser excelentes propostas, mas como entusiasta na area, gostava de ver algo diferente.
- O IC form é simplesmente a questionar quais são as escolhas do cliente. Não me parece que seja um IC para receber feedback que possa ser incorporado para o GB, o que é pena. Alias, o GB é apontado para menos de 1 mês após o lançamento do IC, o que valida o que disse anteriormente.

### O que gostava de ver alterado

- O weight apenas é disponibilizado em alumínio (apesar de ter diversos acabamentos). Usar o mesmo material para o weight anula grande parte do propósito de usar weights.
	- De referir, que com a utilização da espuma, o efeito do weight já seria também diminuído, e provavelmente é indiferente o weight ser em alumínio ou outro material.
- Gostava que houvesse uma maior unificação das opções da PCB e que não houvesse apenas PCBs em hotswap.
	- Também não percebo a insistência em per-key RGB PCBs. Existe assim tanta demanda do mercado?
- O layout suportado parece-me um 65% tradicional. Visto ser um teclado para massas, será que suporta outras PCBs para 65%?
- Não tenho certeza mas parece-me que a versão bluetooth não suporta o firmware ZMK e usa apenas software proprietário, o que não é o ideal.
- Gostava de ver uma foto do teclado montado, que não consegui ver em todas as fotos disponibilizadas.
- Apenas vi uma foto da PCB, gostava de ver mais para perceber a diferença.


## Group Buy

Link: [Site Official](https://qwertykeys.notion.site/Cub65-dce0b590359e4a2197a4e3b9aa143978) e [Reddit](https://www.reddit.com/r/mechmarket/comments/13j5yjd/gb_cub65_qk_x_cub_studios_1st_collaborative/)

### PROS

  * Uma simulação rápida no vendor da Europa leva-me a concluir que este teclado custa na casa dos 230€ já com portes de envio. O preço é competitivo, mas não acho que seja incrível para o que oferece.
  * Os projectos anteriores da QK costumam ser bastante sólidos ao nível de acabamentos, acima da média. O Cub65 não deve ser excepção.
  * Múltiplas opções de cores quer para a case como para o _weight_.
  * Múltiplas versões para o _badge_.
    * _Badge_ que esteticamente é semelhante ao que estava presente no TGR Tomo 75.

### CONS

  * Não oferece grande inovação em nenhuma área. É apenas mais um 65% da QwertyKeys.
  * O _weight_ é apenas em alumínio, o que faz com que este perca grande parte do seu propósito. Fica apenas o aspecto visual.
  * Para utilizadores que pretendam diversos layouts (ex. ANSI e ISO), têm que comprar múltiplas PCBs.
  * Só existe PCB em _hotswap_. Não existe versão _solderable_.
  * A nível sonoro, com a espuma e o tapete de silicone no seu interior, tem um perfil sonoro típico de teclados com espuma. Fica a dúvida como se comporta sem esta, sendo que devido a ter que comportar as baterias para a versão sem fios, é provável haver algumas reverberações ou problemas similares no seu interior.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** Se gostas do _design_ e pretendes um teclado 65% _entry-level_ que ofereça uma proposta de valor consistente, o Cub65 é uma muito boa opção. Não acho que seja a melhor proposta da QK e o vendor na Europa é a Candykeys, que trará provavelmente algumas dores de cabeça na altura da entrega do teclado. No entanto, acaba por ser uma proposta sólida no geral. Aconselho a ver alguns dos meus comentários na altura do IC para perceberem melhor algumas das limitações deste teclado (está apenas umas linhas acima).

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.