# Neo65

![](assets/images/20230725-project-qk-neo-65.jpeg)

**Data do último update:** 03/08/2023
**Maker:** QwertyKeys

## Interest Check

IC realizado antes desta iniciativa.

## Group Buy

Link: [Página Oficial do Produto](https://qwertykeys.notion.site/Neo-65-6c8ae7895ec442dea809057c3dc5e113)

### PROS

  * Um 65% _budget_ com algumas pequenas inovações para ter um preço baixo.
  * _Design_ com apenas uma peça para o teclado e depois um pequeno _weight_ externo.
  * _Weight_ disponível em vários materiais _premium_ como latão, cobre ou aço inoxidável com acabamento PVD.
  * _Design_ à volta da não utilização de espuma.
  * PCB sem _flex_ _cuts_
  * Suporte de QMK e VIA.
  * Suporte de _layout_ ISO (com a _solderable_ PCB).
  * Vários materiais disponíveis para a _plate_.
  * _Plate_ sem _meme_ _cuts_.
  * Várias cores disponíveis.
  * Mala de transporte incluída.
  * Promessa de envio para os diversos _vendors_ apenas 4 semanas após a venda.
  * O preço de 121€ já em território europeu é muito apelativo.

### CONS

  * ~1 Kg de peso é um valor baixo, mas que se compreende tendo em conta o _design_ do teclado e a sua audiência alvo.
  * Utilização de USB _daughterboard_ proprietárias e com conectores que de momento não são _standard_.

### Recomendo comprar?[^1]

- [x] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** O Neo65 é provavelmente o melhor teclado de 65% na actualidade que se pode comprar em território europeu até aos ~120€. Isto já é dizer muito sobre o valor do Neo65. Para o preço, o qual é o aspecto mais forte deste teclado, temos direito a um teclado com a qualidade da QK, um pequeno _weight_ externo em latão (outros materiais estão disponíveis), desenhado para não se utilizar espuma, sem _flex_ ou _meme_ _cuts_ quer na PCB como na _plate_, suporte de ISO, uma mala de transporte incluída e ainda é enviado para o consumidor final no prazo de 4 semanas. Se quisermos outros tipos de acabamento, irá custar um pouco mais, mas é impossível negar que o preço é muito bom. O resultado final ainda é um pouco desconhecido, irá ser necessário esperar pelas primeiras _reviews_ ou experimentar mesmo uma unidade, mas o Neo65 tem todos os ingredientes que considero essenciais para ser um produto com qualidade. Se procuras um 65% com _design_ simples e estás a entrar no hobby, o Neo65 é a minha recomendação de momento!

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.