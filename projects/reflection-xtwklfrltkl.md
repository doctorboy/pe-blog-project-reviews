# Reflection
A XTWKLFRLTKL board

![](assets/images/20230523-project-reflection-xtwklfrltkl.jpeg)

**Data do último update:** 22/05/2023
**Maker:** Han Boards

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120288.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 0     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 1     |
| **Total**            | **6+1** |

### O que aprecio

  * _Layout_ fora do comum e um nome particular, XTWKLFRLTKL.
    * XT — _Layout_ _extended_, representado pelas duas colunas extra no lado esquerdo.
    * WKL — _Winkeyless_, sem a tecla do "Windows".
    * FRL — _F-row-less_, sem a fila das teclas F.
    * TKL — _Layout_ TKL.
  * Uso de um _weight_ externo e interno. Materiais _premium_, neste caso, cobre.
    * Peso já com valores significativos, 3.2Kg.
  * Dois tipos de _mounting_ que devem proporcionar diferentes sensações ao utilizador.
    * Gostei de haver imagens a tentar esclarecer como os dois tipos de _mounts_ suportados funcionam (ponto bónus).
  * Várias cores disponíveis, que permitem o teclado ter 3 cores:
    * Parte superior da_ case_ disponível em diversas cores.
    * Parte inferior em alumínio cinzento.
    * _Weight_ em cobre, com a cor do cobre.
  * O layout inclui barras de espaço 8u e 10u.
  * Suporte de muitos _layouts_, incluindo ISO.
  * O projecto, em geral, parece bem pensado e em diversos pontos demonstra realismo.
    * Quantidade realista com bom preço alvo.
    * Preocupação do _maker_ em endereçar diversos aspectos do teclado, como os aspectos sonoros, sem seguir o caminho fácil de simplesmente colocar espuma no seu interior. Demonstra interesse pelos detalhes.

### O que não aprecio

  * _Design_ da parte inferior demasiado parecido com o Geonworks Glare TKL que saiu recentemente. Pessoalmente não aprecio quando existem teclados demasiado parecidos a outros num período tão curto de tempo.
  * É mencionado no texto que existem protótipos, mas infelizmente não foram partilhadas fotos destes.

### O que gostava de ver alterado

  * Usa uma _daughterboard_ própria, baseada num projecto _open source_. Pessoalmente preferia que fosse usada a versão _open source, pois_ caso haja problemas será mais fácil substituir.
    * Pelas imagens a _daughterboard_ parece-me uma cópia da versão [Unified C3](https://github.com/Unified-Daughterboard/UDB-C-Legacy), mas apenas com outras dimensões físicas.


  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.