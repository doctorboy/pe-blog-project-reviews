# Revanche TKL

![](assets/images/20230530-project-revanche-tkl.jpeg)

**Data do último update:** 30/05/2023
**Maker:** @lewynlight (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120397.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 0     |
| Protótipos           | 1     |
| Mounting visível     | 0     |
| Layouts suportados   | 0     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **4** |

### O que aprecio

- Objectivos definidos desde início para o projecto: uso de policarbonato, preço alvo baixo, etc.
- Uso de USB _daughterboard_ que é _open source_.
- Compatível com PCBs TKL bastante populares, como a Hiney h87A.
- Dois tipos de _mount_, _top-mount_ e _O-ring_.
- PCB do projecto não tem _flex cuts_.
- O preço parece-me aceitável para MOQ de 50 unidades e um limite de 100 unidades.
    

### O que não aprecio

- Não é partilhada informação sobre as diversas _plates_ que o teclado usa, apesar de serem mencionadas várias vezes como sendo um dos aspectos diferenciados do projecto.
- Não suportar _layout_ ISO ao nível de _plates_. A desculpa de que afecta a parte sonora não me parece razoável.
    - Deve ser compatível com plates do Jane V2.

### O que gostava de ver alterado

- Era mais interessante que as plates com propósito acústico tivessem uma grossura superior a 2mm, na minha opinião.
- Falta muita informação sobre o projecto, principalmente sobre as partes interiores.
- Percebo a ideia de usar acabamentos mais baratos para manter os custos mais baixos dos protótipos, mas para um criador menos experiente, pode ser complicado depois perceber e garantir qual será a qualidade final do produto. Um protótipo final deve representar a qualidade final do produto, esse é um dos propósitos de criar protótipos.

  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.