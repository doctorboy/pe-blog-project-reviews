# SAAG65

![](assets/images/20230512-project-saag-65.jpeg)

**Data do último update:** 12/05/2023
**Maker:** Moonkbd

## Interest Check

IC realizado antes desta iniciativa.  

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120272.0)

### PROS

-   Gosto bastante do design, que a nível do profile lateral é praticamente uma cópia do Spectacle 60%.
-   Tem um weight interno e externo.
-   Ao nível de mounting e som é semelhante a um Bakeneko, que foi a base deste projecto.
-   O-Ring mouting oferece algumas garantias de um bom desempenho.
-   Plate sem flex-cuts, sem invenções.
-   Usa uma versão open-source da daughterboard.
-   Boa oferta de materiais para a plate.
-   PCBs é compatível com outros teclados Bakeneko 65.
    -   Será que case é compatível com outras PCBs Bakeneko 65?
-   O preço original parece justo, no entanto, não tem proxy na EU.

### CONS

-  EU sem proxy, o que implica custos de importação em cima do custo do teclado.
    -   Info: A transportadora parece ser a UPS.
-   PCB hotswap apenas suporta ANSI.
-   Primeiro teclado deste maker, vendido em loja própria, com um limite de 350 unidades: poderá trazer alguns desafios ao nível de logística e controlo de qualidade que o maker pode não ter capacidade para endereçar.

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O projecto tem vários aspectos que acho interessantes, mas o facto de não ter proxy EU destrói qualquer tipo de relação preço-qualidade que exista. Caso este custo extra, que irá transformar um teclado de aprox. 220€ em 300-250€ seja aceitável para si, e ande à procura de um Bakeneko 65, tem aqui uma versão com um design bastante particular que em termos estéticos passara dificilmente despercebido.

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.