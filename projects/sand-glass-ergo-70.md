# Sand Glass Ergo 70%

![](assets/images/20230524-project-sand-glass-ergo-70.jpeg)

**Data do último update:** 24/05/2023
**Maker:** Fox Lab

## Interest Check

IC realizado antes desta iniciativa.  

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120295.0)

### PROS

  * O _design_ é bastante particular (combina duas cores) e acho pessoalmente interessante ter a opção de um _wrist rest_23.
  * Aspecto inovador: um _layout_ ergonómico com três ângulos, 0º, 6º e 12º.
  * Disponibilizar PCBs _hotswap_ e _solderable_.
  * Os preços das PCBs como extra parecem-me interessantes.
  * Fox Lab é um criador conhecido.
  * A venda tem proxy na Europa através do melhor vendor europeu na actualidade, a Eloquent Clicks.

### CONS

  * A utilização por defeito da "_skeleton plate_" vai fazer com que muita gente necessite de adicionar uma plate com _design_ tradicional à compra.
  * _Wrist rest_ de metal criam para a maioria das pessoas uma sensação de desconforto. O ideal é optar pela versão de fibra de carbono, no entanto, o preço é mais elevado.
  * Não contém nenhum _weight_, quer seja interno ou externo.
  * Para o propósito de vender algumas centenas de unidades (venda limitada a 400 unidades), o preço de 520€ (sem portes de envio) parece-me ligeiramente elevado para um teclado que apesar de ser composto por 3 peças, nenhuma delas é composta por um material ou acabamento _premium_.
  * Utilização de várias camadas de espuma no seu interior.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O Sand Glass Ergo é provavelmente um dos teclados ergonómicos mais interessantes dos últimos tempos. A utilização de três diferentes ângulos para as teclas e toda a sua construção, fazem-me acreditar que o produto final será algo bastante interessante de utilizar (pessoalmente gostava de utilizar uma, um dia no futuro). O projecto no geral, tem alguns pontos fracos de relevo, como a utilização por defeito de uma _plate_ com imensos _cutouts_ e a utilização por defeito de espuma no interior. Mas se esses aspectos não foram críticos, será certamente um bom ergonómico para se ter por casa.  
Para quem tiver interesse em saber mais, recomendo a visualização da [stream do LightiningXI](https://www.youtube.com/watch?v=WurxLSjxfpE).


[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.