# Sin65

![](assets/images/20230818-project-sin65-wind-studio.jpeg)

**Data do último update:** 18/08/2023
**Maker:** Wind Studio

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=121062.0) e [Geekhack 2](https://geekhack.org/index.php?topic=121063.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     | 
| Protótipos           | 0     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **4** |

### O que aprecio

  * Algumas ideias para tentar inovar no _layout_ de 65%, como, por exemplo, ter a tecla “Esc” isolada.
  * Dois tipos de _mounting_ disponíveis: _top_ _mount_ e PCB _gasket_ _mount_.
  * Possibilidade de ter opção _plateless_ ao usar o _gasket_ _mount_.
  * Suporte de ISO na _solderable_ PCB.
  * Utilização de alumínio 6063.
  * A utilização de um _weight_ externo.

### O que não aprecio

  * A não existência de um IC form para recolher _feedback_ de modo mais estruturado.
    * Para tornar as coisas ainda mais confusas, criaram dois tópicos no Geekhack.
  * O _weight_ externo é bastante pequeno, o que num teclado com um _design_ de dimensões generosas para um 65%, deixa-me um pouco desiludido.
  * Tendo em conta que o teclado é produzido por um criador na China e já com alguma dimensão, acho o preço demasiado elevado ($288), pois certamente este teclado irá ser vendido em grandes quantidades no mercado asiático.
  * A oportunidade de se usar _weights_ internos que não é aproveitada.

### O que gostava de ver alterado

  * Nas partes internas, em vez de ser utilizado _weights_ (quando não se usa as baterias) o criador optou por colocar espuma. Neste caso a espuma é mesmo colocada em _cutouts_ que o teclado tem internamente.
  * Gostava de ver mais informação sobre a USB _daughterboard_ utilizada.

  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.