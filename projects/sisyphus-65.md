# Sisyphus 65

![](assets/images/20230524-project-sisyphus-65.jpeg)

**Data do último update:** 13/06/2023
**Maker:** Velocifire a liderar um projecto da Maniac Rabbit Sudio

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120287.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 0.5   | 
| Protótipos           | 1     |
| Mounting visível     | 0.5     |
| Layouts suportados   | 0     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **5** |

### O que aprecio

- _Design_ fora do comum, apesar de pessoalmente não apreciar de todo.
- Acho interessante algumas das cores nos _renders_. Fica a dúvida como serão ao vivo.
- O protótipo tem melhor aspecto que os _renders_ disponibilizados.

### O que não aprecio

- Não aprecio esta tendência de ter teclados, com o _mounting_ na PCB, mas depois como só disponibilizam PCBs _hotswap_, são obrigados a utilizar _plates_. Parece-me uma escolha desajustada ao nível de _design_. Na minha opinião, se os _mounting points_ estão na PCB, é porque a filosofia a seguir deve ter a sua base num conceito _plateless_, e por defeito a PCB devia ser _solderable_ e com suporte a _multi-layout_.
- Não é muito claro como é feito o _mounting_ (apenas a foto de _exploded view_ mostra um pouco). A PCB tem pontos de _mounting_ laterais, num _mounting_ que me parece ser do tipo _gasket_, o que não é bom sinal.
- A plate tem os chamados "_meme" cuts_ que não parecem ter grande finalidade. A foto da plate FR4 parece ter alguns buracos com furos escareados, o que não faz grande sentido numa plate FR4.
- O preço, apesar de parecer-me aceitável, pessoalmente acho que é demasiado elevado para o que é oferecido em comparação com outras _budget_ boards (ex. QK65).
    

### O que gostava de ver alterado

- Não ser apenas _hotswap_ e suportar mais _layouts_, no mínimo ISO.
    - Um pormenor: suportar VIAL, para além de QMK e VIA, seria o ideal.
- O _weight_ é uma pequena plate exterior apenas, que pouco mais acrescenta do que benefícios estéticos.
- É mais um pormenor: a PCB tem _flex cuts_ que não são uniformes para todas as teclas alfanuméricas, neste caso, na primeira linha a contar da parte inferior, o corte é interrompido em dois locais sem razão aparente.

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120498.0)

### PROS

  * Apesar de pessoalmente não apreciar a estética do Sisyphus65, este é um dos pontos de destaque.
    * Sempre interessante ver quando imagens e vídeos dos protótipos têm melhor aspecto que as renderizações anteriormente partilhadas.
  * Um _weight_ em aço inoxidável com revestimento PVD.
  * Ter um revendedor na Europa, a Keygem.

### CONS

  * Ter um *design* e _mount_ pensado em _plateless_, mas depois não ter opção _plateless_.
  * Os _weights_ serem de pequenas dimensões e mais decorativos que outra coisa: uma pequena plate na parte inferior e o cilindro que faz o ângulo do teclado.
  * As _plates_ terem os chamados "meme" cuts que não parecem ter grande finalidade, principalmente quando o _mounting_ é feito pela PCB.
  * A PCB tem _flex cuts_ que não são uniformes para todas as teclas alfanuméricas, neste caso, na primeira linha a contar da parte inferior, o corte é interrompido em dois locais sem razão aparente.
  * Não utiliza uma USB _daughterboard, _mas utiliza um gasket mounting com _flex cuts_, o que poderá ser problemático a longo prazo (dependendo da flexibilidade permitida, poderemos danificar a porta USB).
  * Ter apenas disponível uma PCB _hotswap_ e não suportar maior variedade de _layouts_, como, por exemplo, ISO.
  * O preço não é competitivo quando temos já oferta mais completa com o [Jris65](jris65-r2-limited-edition.md) ou o [Cub65](qk-cub65.md).


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [x] Não recomendo

**Comentário:** A principal proposta do Sisyphus65 é o seu _design_ diferenciado, algo que faz bastante sentido no mercado de 65% visto que este é bastante explorado e existe praticamente oferta para todos os gostos. No entanto, considerando algumas das escolhas realizadas pelo criador e considerando o seu preço no mercado europeu (a partir de 335€), não consigo recomendar o Sisyphus65. As escolhas feitas ao nível da PCB (ser só _hotswap_, não suportar ISO, _flex cuts_ inconsistentes, ausência de USB _daughterboard_) deixam bastante a desejar. A cereja no topo do bolo é todo o teclado ser baseado num _gasket mounting_ utilizando a PCB, que "grita" por todo o lado para ser usado como _plateless_, mas depois tal não é possível porque não é oferecida a opção de usar uma _solderable_ PCB. Se o *design* é tudo para ti e adoras a estética do Sisyphus65, é provável que este não te desiluda. Mas é inegável que de momento existem ofertas mais interessantes e versáteis no mercado, como o [Jris65](jris65-r2-limited-edition.md) ou o [Cub65](qk-cub65.md)..

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.