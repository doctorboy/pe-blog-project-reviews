# Sonic170

![](assets/images/20230626-project-sonic-170.jpeg)

**Data do último update:** 26/06/2023
**Maker:** @homiezoned (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120635.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 0.5   |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **4.5** |

### O que aprecio
  * Um _design_ pouco convencional, mas que vai de encontro com o que o criador queria realizar, um projecto que fosse diferente e, ao mesmo tempo, divertido.
  * Utilização de alumínio 6063.
  * A utilização de uma USB daughterboard open source.
  * Observar que o criador já tem na sua posse alguns protótipos, algo fundamental quando se avança com um _design_ fora do comum.
  * Não sou fã de _flex cuts_ em PCBs, mas os que são apresentados parecem ser aceitáveis, onde "isolam" teclas alfanuméricas.
  * A existência de um botão rotativo no perfil lateral direito do teclado. Fora do comum; a sua praticabilidade vai depender muito do _knob_ utilizado.
  * Dois layouts distintos: 65% com _blocker_ ou 65% WKL.
  * Na venda irá ter um distribuidor na Europa, o que é de realçar.

### O que não aprecio
  * Não tem um formulário para receber feedback de modo estruturado.
  * Apesar do criador chamar o _mounting_ de "_silicone gasket leafspring_", pelo que consigo observar na _exploded_ _view_ trata-se de um simples _gasket_ visto que as peças de silicone assentam na totalidade quer na plate como na _case_.
  * A plate tem um _design_ que tem ultimamente estado na moda e que tem imensos _cutouts_. Este aparenta ser o único _design_ disponível, e antecipo que não seja agradável de utilizar em materiais como o policarbonato.
  * Numa primeira análise visual do teclado, fiquei com a esperança que houvesse algum tipo de teclas sensíveis ao toque na parte direita do teclado. Mas estas não existem e são apenas _designs_ visuais.

### O que gostava de ver alterado
  * A utilização de duas peças inferiores em alumínio é uma oportunidade perdida em utilizar outro tipo de material que permitisse transmitir um maior requinte e melhores características sonoras (na teoria).
  * O suporte ao _layout_ ISO não é a 100%, falta-lhe o _split left shift_. No entanto, o criador pareceu interessado em ajustar essa compatibilidade para suportar ISO.
  * As _plates_ não suportam _layout_ ISO por defeito, e visto que o projecto ambiciona suportar PCBs com essa capacidade, o ideal seria ter como opção uma _plate_ universal.
  * O preço alvo não aparenta ser o melhor, mas é preciso ter em conta que terá certamente um número baixo de unidades numa venda internacional e o teclado em si é composto por várias peças. O preço seria mais justo se uma das partes inferiores fosse num material diferente, como cobre ou latão.
  

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120848.0), [Página da Keygem](https://keygem.com/products/sonic170-keyboard-kit-group-buy)

Link: [Geekhack](https://geekhack.org/index.php?topic=120848.0), [Página da Keygem](https://keygem.com/products/sonic170-keyboard-kit-group-buy), [Review do IC](https://perifericosdeescrita.substack.com/i/131259526/sonic)

### PROS

  * Um projecto diferente do convencional. É de louvar sempre estes projectos na minha óptica, desde que apresentem alguma qualidade.
  * Foi adicionado o suporte ao layout ISO na sua totalidade.
  * Dois _layouts_ disponíveis: 65% com _blocker_ ou 65% WKL.
  * A existência de um botão rotativo no perfil lateral direito do teclado que é uma característica fora do comum.
    * Suporte de QMK, VIA e Vial.
  * USB _daughterboard_ que parece utilizar o mesmo _form_ _factor_ que modelos _open_-_source_.
  * A utilização de _weight_ em aço inoxidável.
  * Utilização de um vendedor europeu, é de louvar, ainda para mais num projecto de nicho.

### CONS

  * A _plate_ estar cheia de _meme_ _cuts_.
  * Utilização de espuma por defeito.
  * O _weight_ ser apenas externo e de muito pequena dimensão.
    * Tem uma espécie de gravação a laser interna que apenas será visível se desmontarmos a case do teclado na sua totalidade.
  * A utilização de uma peça intermédia em alumínio, é uma clara oportunidade perdida em utilizar outro tipo de material que permitisse transmitir um maior requinte e melhores características sonoras, tal como foi recentemente feito no Glare 65.
  * O preço de 410€ é na minha opinião demasiado elevado para o que é oferecido.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [x] Não recomendo

**Comentário:** O Sonic170 é um teclado para todos os que procuram algo fora da caixa, no aspecto estético. Tem uma boa base sólida de trabalho, com o suporte de duas variações de layout e um botão rotativo lateral original. No entanto, é uma pena que depois o projecto falhe em aspectos mais básicos, como utilizar uma plate cheia de _meme_ _cuts_ e também não apresente um preço competitivo para o que é oferecido. Se o criador tivesse utilizado outro tipo de material numas das partes inferiores do teclado, teria, a meu ver, lançado este projecto para outro nível e poderia até pedir ainda um valor mais elevado. O Sonic170 é um teclado marcado essencialmente pela sua estética distinta e pela presença de um botão lateral rotativo. Se é algo que valorizas e gostas da estética, o Sonic170 será um teclado certamente engraçado de se utilizar. Se esses aspectos não forem valorizados, o meu conselho é aguardar por outros projectos mais sólidos que certamente irão aparecer no futuro.

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.