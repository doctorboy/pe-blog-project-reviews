# Split60
A split 60% board HHKB/WK

![](assets/images/20230509-project-split60.jpeg)

**Data do último update:** 09/05/2023
**Maker:** OldMate (same designer than Let's Tango)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120247.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 0     |
| Layouts suportados   | 0     |
| Preço Estimado       | 0     |
| Bónus                | 0     |
| **Total**            | **4** | 

### O que aprecio

-   Usar alumínio reciclado, o que é uma opção interessante.
-   Tentar fazer algo que não existe no mercado, um split 60% HHKB em alumínio.

### O que não aprecio

-   Não ver a parte interna do teclado, PCBs ou plates.
-   Sem suporte para ISO.

### O que gostava de ver alterado

-   Percebo que ainda haja muitas variáveis em jogo, mas era importante partilhar um preço alvo, consoante as opções.
-   Gostava de ver mais algumas specs, ex. qual a altura frontal, porquê usar 3mm na plate, etc.

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.