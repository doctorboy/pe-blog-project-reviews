# Tequen 65

![](assets/images/20230808-project-tequen-65.jpeg)

**Data do último update:** 08/08/2023
**Maker:** Mighty Setup (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120995.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 0.5     |
| Specs                | 1     |
| Protótipos           | 0     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 0     |
| Bónus                | 0     |
| **Total**            | **4.5** |

### O que aprecio

  * Utilização de materiais mais _premium_, como o alumínio 6063 e o _weight_ e _frame_ superior em aço inoxidável ou latão.
  * Dois tipos de _mounting_, _gasket_ _mount_ e _top_ _mount_.
  * Utilizar um conceito que está a tornar-se moda, os HG _sides_.
  * Disponível duas PCBs, uma _hotswap_ e outra _solderable_, com suporte de ISO e _split_-_spacebar_.
    * A não existência de _flex_ _cuts_ nas PCBs ou na _plate_.
  * Dá a entender que se utiliza uma USB _daughterboard_ _open_ _source_ (é necessária confirmação).
  * Design que reúne algumas opções menos comuns, ex. a utilização de uma _frame_ superior para dar duas tonalidades ao teclado.

### O que não aprecio

  * O fabricante ser a KBDfans, que tem um histórico de não apresentar a melhor qualidade nos seus produtos.
    * Têm também fama de já ter prejudicado alguns designers no passado.

### O que gostava de ver alterado

  * Pessoalmente não fiquei muito entusiasmado com a localização dos _mounting_ _points_ para a plate. De referir que com o _mounting_ em _gasket/leafspring_ não se deve notar praticamente diferença nenhuma.
  * Projecto ainda numa fase inicial, sem protótipo e estimativa do preço alvo.
  * O _design_ da _plate_ que tem demasiados _cutouts_.


## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.