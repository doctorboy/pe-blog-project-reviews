# TKD Cycle7

![](assets/images/20230524-project-tkd-cycle7.jpeg)

**Data do último update:** 26/07/2023
**Maker:** TKD, junção da Vertex e Equalz

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120373)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 1     |
| IC Form c/ perguntas | 1     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **7** |

### O que aprecio

  * Um _layout_ menos convencional, mas que pessoalmente acho que tem bastante potencial.
  * Uso de alumínio 6063.
  * Uso de _weights_ internos e externos, sendo que os internos são utilizados para cobrir as baterias. O _weights_ externos são em aço inoxidável.
  * _Flex cuts_ na PCB que isolam a zona das teclas alfanuméricas. Não sendo fã de _flex cuts_ na PCB, os propostos no Cycle parecem-me interessantes.
    * Proporcionam também tiras de, ao que parece ser, borracha, para colocar nos _flex cuts_ na PCB e melhorar o som.
  * A _case_ é fixada com um sistema original, denominado como "_EZ assemble system_". Fica a dúvida sobre a durabilidade do sistema, e se ao fim de algumas utilizações se vai haver folgas.
  * Duas opções para o _mounting_, que proporcionam duas sensações diferentes ao teclar.
  * Preocupação do criador em optimizar aspectos sonoros do teclado sem utilizar espuma, é sempre de louvar.
    * Oferecer espuma como opção para quem quiser usar, apesar de o teclado não ter sido desenhado com isso em mente.
  * PCBs disponíveis em versões _hotswap_ e _solderable_, tendo esta última a opção de layout ISO.
  * O preço é muito interessante.

### O que não aprecio

  * O posicionamento dos _weights_ internos (devido à posição das baterias), não está centralizado com as teclas alfanuméricas, o que poderá na teoria criar algumas não uniformidades a nível sonoro nessas teclas.

### O que gostava de ver alterado

  * Gostava de ver informação sobre alguns aspectos que não foram mencionados, como o peso ou a altura frontal.


## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120373.0), [Fotos do Markerchun](https://markerchun.com/cycle7), [Vídeo do Markerchun](https://www.youtube.com/watch?v=vmil0ALoFtY)

### PROS

  * Um TKL FRL com um preço super competitivo para o que oferece.
  * Uso de materiais _premium_ nos diversos componentes do teclado.
    * Alumínio 6063 na case.
    * Latão nos _weights_ internos.
    * Aço inoxidável no _weight_ externo.
  * Algumas escolhas de design bastante interessantes como, por exemplo, usar os _weights_ internos para cobrir as baterias.
  * Disponibilizar pequenas tiras de borracha/silicone para colocar nos _flex_ _cuts_ na PCB e deste modo tentar eliminar os efeitos sonoros indesejáveis dos _cutouts_ na PCB e manter ainda alguma da flexibilidade.
    * Parece-me, no entanto, que não disponibilizam para todos os flex cuts.
  * O sistema _mount_ da case, é bastante fácil de utilizar e prático para quem está sempre a realizar alterações no seu teclado.
	  * Sistemas semelhantes já foram utilizados anteriormente em _boards_ como o Matrix Lab 6XV 3.0 Corsa ou o Jris75.
  * Os dois sistemas de _mounting_ que proporcionam duas sensações distintas ao teclar.
  * A preocupação do criador em refinar alguns aspectos do teclado tendo em conta a não utilização de espuma.
  * O nível do acabamento é óptimo para o preço pedido. O feedback geral das pessoas que têm feito _reviews_ do teclado é que a qualidade da anodização é bastante boa nos protótipos.
  * Boa oferta de PCBs e _layouts_ suportados (incluí ISO).
  * Mala de transporte incluída (também incluí _stabs_ e _bumpons_ personalizados no kit).
  * Disponível num vendedor europeu.

### CONS

  * Alguns dos _flex_ _cuts_ na PCB não fazem grande sentido, como, por exemplo, o _flex_ _cut_ em cima das teclas com as setas.
  * Bastantes dúvidas sobre a durabilidade do sistema “EZ assemble system” utilizado no _mount_ da _case_ (sistema “ball catch latch”). É quase inevitável que o sistema ganhe folgas ao longo do tempo, apesar da sua praticabilidade.
    * Diria que em casos extremos, dará para substituir as peças do _mount_.
  * Utilização de uma USB _daughterboard_ que não é _open_ _source_.
  * Uma falha, a meu ver, ao nível de design são as folgas que existem na parte lateral, que quando o teclado está por montar, e apenas nesta situação, consegue-se ver o exterior.
    * Seria OK, se fosse algo premeditado ou parte do design original, mas da maneira como está parece-me apenas uma parte do teclado que ficou por refinar.


### Recomendo comprar?[^1]

- [x] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** O TKD Cycle 7 é a _board_ do momento. Aquela que o _Blacksimon_ alega como a grande disruptora no _hobby_ e que irá “destruir” todos os criadores de pequena e média dimensão no _hobby_. Pessoalmente não entro nesses exageros, e acho que já nos últimos 12 meses houveram lançamentos de teclados com uma proposta de valor semelhante. Por exemplo, os últimos projectos da QK, tiveram propostas de valores muito semelhantes, a grande diferença é que alguns criadores de conteúdo decidiram ignorar estes teclados, não sei bem por que razão, e agora vendem a imagem de que a TKD está a inventar a roda.  
Dito isto, o TKD Cycle 7 é uma excelente proposta e tem tudo para se tornar uma referência não só no seu _layout_, mas também no _hobby_ em geral. Na minha opinião, os 3 principais pontos que o Cycle 7 traz para se diferenciar da concorrência são, a qualidade dos acabamentos disponibilizados (fica ainda por avaliar a qualidade do _coating_ proprietário), o preço e o sistema de montagem da _case_. É neste último que recai as minhas maiores dúvidas e onde apenas o tempo irá dizer se é um bom sistema ou não. As vantagens que traz ao nível de praticabilidade, pessoalmente, é algo que não valorizo, ainda mais se estas implicarem depois a necessidade de algumas _mods_ extra para resolver problemas criados por este _mount_ (a própria _case_ já vem de origem com algumas tiras de _poron_ para melhorar o encaixe e possivelmente ajudar a eliminar vibrações). No entanto, isto são pequenos pormenores num teclado que me parece ser de grande qualidade, certamente bastante acima do seu preço base. No entanto, acho que será exagerado e mesmo ingrato para o Cycle 7, dizermos que este se compara a teclados de gamas mais altas. Seria, por exemplo, difícil de aceitar que um Geon F1-8X tivesse as folgas laterais que o Cycle 7 tem. No entanto, olhando para o preço, é uma proposta de valor muito boa e uma das melhores do ano certamente. Se és alguém que está a entrar no _hobby_, que gosta do _layout_ e que não quer gastar mais de 200-250€ num teclado, o Cycle 7 é provavelmente a melhor escolha de momento. Se, por outro lado, já estás envolvido bastante no _hobby_ e procuras experiências mais personalizadas e refinadas, diria que o Cycle 7 não tem muito a acrescentar. Quem sabe se um dia tenho a possibilidade de experimentar o Cycle 7 com mais detalhe e a minha opinião mude, mas de momento é o que penso do Cycle 7.


[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.