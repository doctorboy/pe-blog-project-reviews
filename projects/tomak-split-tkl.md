# Tomak
Tented Split TKL

![](assets/images/20230717-project-tomak-split-tkl.jpeg)

**Data do último update:** 17/07/2023
**Maker:** [Syryan](https://www.srind.kr/) (Geekhack)

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=120835.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 1     |
| Mounting visível     | 0     |
| Layouts suportados   | 1     |
| Preço Estimado       | 0     |
| Bónus                | 1     |
| **Total**            | **4** |

### O que aprecio

  * Um TKL _split_ com inclinação de cada lado será sempre um projecto de nicho, mas interessante de ser ver.
  * O peso de 3.5 Kg para um TKL, ainda para mais num projecto deste estilo, é impressionante.
  * Utilização de alumínio 6063.
  * Um _weight_ em latão que será interno e externo.
  * A PCB ser desenvolvida pelo ai03 é na teoria um indicador da qualidade da mesma.
  * Todos os desenhos técnicos partilhados. Bem interessantes e elucidativos (ponto bónus).

### O que não aprecio

  * Não ser disponibilizado um IC _form_ para recolher feedback de modo estruturado.
  * O pormenor de adicionar uma coluna extra com as letras B, H, Y e 7 no lado esquerdo, ficando assim disponível dos dois lados para haver uma melhor adaptação a este layout (normalmente apenas é adicionado a tecla B) é no papel uma boa ideia, mas tem um grande problema: estas teclas, à excepção do B, não são disponibilizadas em duplicado quando compramos _keysets_, o que torna obrigatório comprar dois _base_ kits para colocar neste teclado.
    * Uma possível solução será utilizar teclas brancas ou _keysets_ como o GMK Dots.
  * A PCB apresenta _flex_ _cuts_ que não são uniformes, o que é desapontante.
  * Utilização da espuma no seu interior. Um projecto com este nível de requinte, e ambição, merecia que fosse pensado de raiz para não utilizar espuma no seu interior.

### O que gostava de ver alterado

  * Gostava de ver melhor como é realizado o _mounting_.
  * Apenas disponibilizar uma PCB _hotswap_. Seria melhor se houvesse a proposta de ter também como opção uma _solderable_ PCB.
  * Não suportar o _layout_ ISO. Algo que pode ser corrigido, a meu ver, com a introdução de uma _solderable_ PCB que suporte múltiplos _layouts_.
  * A _bottom_ _row_ não ser simétrica dos dois lados: no lado esquerdo usa _mods_ com 1.5u e do lado direito usa _mods_ com 1.25u.
  * Utilização de um apoio para os pulsos em alumínio. Preferia que este tivesse uma estrutura em alumínio, mas as zonas de contacto com os pulsos fossem num material mais confortável ao toque, como, por exemplo, madeira.
  * Ser disponibilizado um preço alvo, nem que seja com um intervalo de valores.
  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.