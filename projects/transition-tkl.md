# Transition TKL

![](assets/images/20230613-project-transition-tkl.jpeg)

**Data do último update:** 13/06/2023
**Maker:** Swagkeys (sem certezas)

## Interest Check

IC realizado antes desta iniciativa.    

## Group Buy

Link: [Página Oficial](https://swagkeys.notion.site/Transition-TKL-d39841e7347442c7a6b8b778ccdc0c2b), [Album do Markerchun](https://markerchun.com/transition)

### PROS

  * Duplo _weight_, um deles interno e externo e outro apenas externo.
  * Ao nível de *design* é um TKL bastante bonito. As cores são também muito interessantes.
  * Três tipos de _mounting_ disponíveis: _top mount_, _gasket mount_ e _sandwitch mount_ (pouco comum).
  * Pelas imagens parece ser um teclado bastante refinado ao nível de engenharia/_design_, com muita atenção aos detalhes.
  * Bom preço base de $400 para o que é oferecido (incluindo PCB e _plate_).
  * Bons preços para os extras de _plates_.

### CONS

  * Ausência de suporte ao _layout_ ISO, mesmo na _solderable_ PCB.
  * As PCBs disponibilizadas podiam utilizar já os novos conectores _molex_ para ligar às USB _daughterboard_, que em breve serão o novo _standard_, substituindo os conectores JST.
  * Não é disponibilizado nenhum revendedor ou distribuidor na união europeia, o que significa que o preço final em terras lusas será bastante mais elevado.
  * Tendo em conta o tamanho e os vários _weights_, esperaria um peso mais elevado perto dos 3Kg, mas o peso final é de 2.3Kg (o que não deixa de ser bom).

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O Transition TKL é um projecto muito interessante, em que é bastante óbvio que houve muita atenção do criador em tentar criar um produto final que não fosse apenas bonito esteticamente, mas também uma boa proposta de valor nas restantes áreas. Um dos pontos de destaque, é a tripla opção de mounting disponível, com destaque para o menos comum (e também menos normalmente apreciada) _sandwitch mount_. O principal ponto fraco deste teclado é a sua completa ausência de suporte ao layout ISO (nas PCBs e _plates_) e a ausência de um revendedor na Europa, que acaba por destruir um dos pontos fortes deste teclado, o preço. Se o layout ISO não lhe diz nada, e não se importa de despender algo na casa dos 600-700€, o Transition certamente que o fará muito feliz.

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.