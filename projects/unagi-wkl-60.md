# Unagi 60%

![](assets/images/20230531-project-unagi-wkl-60.jpeg)

**Data do último update:** 31/05/2023
**Maker:** Geo da YVKB

## Interest Check

IC realizado antes desta iniciativa.  

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120422.0)

### PROS

- Tendo em conta o número baixo de unidades, e a utilização de um _weight_ em aço inoxidável que cobre a parte inferior na totalidade, o preço, apesar de alto, parece adequado.
- Preços da PCB e _plates_, muito competitivos.
- Utilizar uma PCB 60% bastante popular, a Hiney h60 e utilizar uma USB _daughterboard_ _open source_.
- Pessoalmente gosto bastante do perfil lateral do teclado.

### CONS

- Não me parece que vá ter um distribuidor na Europa, o que tornará o seu preço completamente desajustado.
- O _cutout_ para o cabo JST está completamente visível e na teoria poderá trazer algumas inconsistências a nível sonoro.
- Após ouvir algum do _feedback_ da _review_ do lightningXI e considerando que tem uma altura frontal bastante baixa, parece-me que a barra de espaços vai trazer alguns desafios a nível sonoro. Não é definitivamente um teclado para iniciantes na minha opinião.

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O Unagi é um 60% de nicho, com muito poucas unidades (max. 30 unidades), um design simples e minimalista, mas que explora o conceito de expor materiais no seu aspecto mais bruto. Acho uma proposta bastante interessante na área, e que certamente irá agradar os amantes de 60%. É uma pena que não haja um vendedor na Europa para o projecto, se não possivelmente a minha recomendação seria "sem dúvida".

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.