# Vanguard65

![](assets/images/20230524-project-vanguard-65.jpeg)

**Data do último update:** 24/05/2023
**Maker:** Desconhecido

## Interest Check

IC realizado antes desta iniciativa.    

## Group Buy

Link: [Geekhack](https://geekhack.org/index.php?topic=120293.0)

### PROS

  * Em termos de _design_, pessoalmente não aprecio muito, mas é um 65% com _knob_ e um _slider_ vertical, o que não é comum.
  * O uso de materiais _premium_, como alumínio 6063 na _case_ e _brass_ no _weight_.
  * O _weight_ é interno e externo.
  * Usa uma USB _daughterboard_ que é _open source_, facilitando a substituição no futuro (com cabo JST).
  * Diferentes materiais disponíveis para a _plate_. O _design_ da plate também me parece simples, sem _meme cuts_ o que é um bom ponto de partida.
  * A versão da PCB _hotswap_ suporta ANSI e ISO.
  * A versão com fundo em policarbonato, e _weight_ em _brass_, parece-me ser uma combinação com bastante potencial.
  * O preço parece-me aceitável tendo em conta que o número de unidades vendidas será baixo e o teclado oferece bastante personalização.

### CONS

  * Em termos de gosto pessoal, não aprecio o _rotary_ _encoder_ estar tão saliente verticalmente do teclado. Preferia um _knob_ menos alto (como pessoalmente usei no [ClackX Alpha](https://clackx.xyz/content/8-clackx-alpha)).
  * Parece-me que apenas disponibiliza a PCB em versão _hotswap_.
  * Não é perceptível se existe um MOQ para este teclado.
  * O vendedor na Europa será a Candykeys, o que poderá resultar em alguns problemas de logística aquando da entrega do teclado no final do ano.
  * O controlo de qualidade proporcionado pelo maker é desconhecido, existindo algum risco de o produto final não corresponder às expectativas. De realçar que o protótipo parece ter bons acabamentos.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** Considerando o preço e materiais utilizados, o Vanguard parece ser um projecto interessante e de nicho para quem procura um 65% com um _design_ diferenciado. Pessoalmente não sou fã do _design_ adoptado para o _slider_ e _knob_, mas todo o projecto parece ser sólido. Talvez a grande dúvida seja qual será a qualidade final do produto e respectivo controlo de qualidade que o criador irá oferecer.

[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.