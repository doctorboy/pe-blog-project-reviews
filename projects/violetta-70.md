# Violetta 70%

![](assets/images/20230530-project-violetta-70.jpeg)

**Data do último update:** 30/05/2023
**Maker:** SoSo

## Interest Check

Link: [Geekhack](https://geekhack.org/index.php?topic=119713.0)

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 1     |
| Protótipos           | 0     |
| Mounting visível     | 1     |
| Layouts suportados   | 1     |
| Preço Estimado       | 1     |
| Bónus                | 0     |
| **Total**            | **4** |

### O que aprecio

- O layout ser diferenciado: um 70% que mantém a F-row, mas "perde" a outra fila de teclas R1.
- Em termos de design, é semelhante às propostas da Matrix Labs, e como tal é uma alternativa.
- Layout de HHKB num 70%, interessante.
- As imensas possibilidades de layouts que o teclado permite.
    - WKL, WK, HHKB, possibilidade de usar qualquer uma das filas R1 no topo, suporte de R2 _backspace_, suporte de ISO, etc.
- Uso de materiais como alumínio 6063 ou aço inoxidável.
- A plate parece ter bons locais para os pontos de _mounting_.
- As várias cores que estão a ser planeadas.
- Preço de $350 parece ser competitivo, principalmente quando comparado com os preços actualmente praticados pela Matrix Labs.

### O que não aprecio

- A zona da USB _daughtboard_ não parece estar ainda refinada, pelas poucas imagens disponíveis.
    - Por exemplo, não vejo um canal para acomodar os cabos da USB _daughterboard_.
- Não tem IC form, nem recolhe _feedback_ com sugestões da comunidade para melhoramentos do projecto.
- Falta muita informação sobre o projecto.
- Falta criar protótipos para fazer evoluir o projecto de maneira positiva.

### O que gostava de ver alterado

- Gostava de ver mais informação sobre a parte interior e sobre a PCB.

## Group Buy

Link: [Página do vendedor europeu](https://www.eloquentclicks.com/product/gb-violetta-%e2%89%a570-keyboard/)

### PROS

  * Um 70% diferenciado, onde a nível estético mantém a uma linha separada no topo como a F-_row_, mas que será utilizada para as teclas numéricas. Numa altura que temos imensos projectos 70%, é bom ver alguém fazer coisas diferentes.
  * Em termos de _design_, ser uma alternativa ao Matrix Labs.
  * Oferecer três layouts: WK, WKL e HHKB. Este último é raro em 70%. Suporta ISO também.
  * Os protótipos têm bom aspecto nos vídeos de promoção e fotos partilhadas.
  * Sem 100% de certeza, mas penso que o teclado permite montar a porta USB quer à esquerda como à direita. Interessante.
  * Sem 100% de certeza, mas parece utilizar uma USB _daughterboard open source_.
  * Utilizar dois _weights_, um interno supostamente em alumínio e outro externo em aço inoxidável.
  * Na configuração sem espuma, o feedback de reviews tem sido positivo.

### CONS

  * Continua a não existir uma página oficial do projecto onde se possa consultar informação com mais detalhe.
  * Não existe um _cutout_ na peça inferior do teclado para acomodar os cabos da USB _daughterboard_.
  * Depois de uma elevada expectativa de ter um preço super competitivo com base no que foi partilhado anteriormente, o preço final de quase 480€ para a versão principal (cor _milk_, _weight_ com acabamento PVD e WKL), acaba por ser uma desilusão.


### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [x] Sim, se...
- [ ] Não recomendo

**Comentário:** O Violetta 70 é uma boa lufada de ar fresco no mercado de FRLs, onde de momento nascem novos projectos quase todas as semanas. O Violetta 70 assenta a sua proposta de valor em dois pontos fundamentais: o _layout_ diferenciado ao manter esteticamente a F-_row_ e permitir "n" variações do _layout_ base incluindo até HHKB; e o seu _design_ inspirado no Matrix Lab com cores vibrantes e muitas peças acessórias. Embora não possua tantos _badges_ e pormenores como os teclados da Matrix Labs, os entusiastas deste estilo têm aqui uma alternativa com uma proposta de valor mais equilibrada e forte, especialmente em comparação com [Matrix Lab Navi](matrix-lab-navi.md). No entanto, não posso terminar esta analise sem referir que fiquei um pouco desiludido com o preço final proposto (e essa é a razão da minha recomendação não ser "sem dúvida"). Com base na informação partilhada no IC, esperava algo na casa dos 400€ e na realidade são praticamente 500€, o que poderá ser difícil de justificar quando o [TKD Cycle7](tkd-cycle7.md) está perto de ser lançado também a um preço supostamente mais competitivo. No final, acho que a escolha do Violetta 70 vai depender muito do gosto estético de cada um de nós e se valorizarmos ou não o seu layout único (que, por sinal, pode ser difícil de utilizar para os amantes de ISO).


[^1]: Se gostas do project e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.