<%* const _file_name = await tp.system.prompt("Nome do ficheiro?") -%>
<%* const _title = await tp.system.prompt("Nome do Projecto?") -%>
<% await tp.file.rename(_file_name) -%>
# <%* tR += _title %>
Subtitulo

PHOTO HERE

**Data do último update:** Adicionar data
**Maker:**

## Interest Check

Link: [Geekhack]()

| IC Checklist         |       |
| -------------------- | ----- |
| IC Link              | 0     |
| IC Form c/ perguntas | 0     |
| Specs                | 0     |
| Protótipos           | 0     |
| Mounting visível     | 0     |
| Layouts suportados   | 0     |
| Preço Estimado       | 0     |
| Bónus                | 0     |
| **Total**            | **0** |

### O que aprecio

-

### O que não aprecio

-

### O que gostava de ver alterado

-

  

## Group Buy

Link: [TBD]()

### PROS

- TBD

### CONS

- TBD

### Recomendo comprar?[^1]

- [ ] Sem dúvida
- [ ] Sim, se...
- [ ] Não recomendo

**Comentário:** TBD

[^1]: Se gostas do projecto e o custo não é problema, avança! Esta review é apenas a minha opinião e recomendação.